{{-- @php
  use App\About;
@endphp --}}

<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from locksternsolutions.com/broccoli/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Feb 2019 17:11:18 GMT -->
<head>
<title>Broccoli - Career</title>
  <meta charset="UTF-8">
  <meta name="keywords" content="HTML,CSS,XML,JavaScript">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Site Icons -->
  <link href="{{config('app.url')}}/assets/img/icon.jpg" type="{{config('app.url')}}/assets/img/Home-512.png" rel="icon">

  <!-- font-icon -->
  <link rel="stylesheet" href="{{config('app.url')}}/assets/font-awesome/css/font-awesome.min.css">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/assets/css/bootstrap.min.css">

  <!-- Custom CSS -->
  <link rel="stylesheet" href="{{config('app.url')}}/assets/style.css">
  <link href="https://fonts.googleapis.com/css?family=Josefin+Sans|PT+Sans" rel="stylesheet">
  <!-- <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet"> -->

</head>
<body>
<!--=========== top head =========-->


<!--=========== end top head =========-->

<!--=========== Navbar section =========-->


 <!--=========== end brand section =========-->

  <!--=========== new section =========-->
  @include('includes.header')
  <!--=========== end new section =========-->

 <!--=========== Slider section =========-->



  <div class="clearfix"></div>
  <!--=========== end Slider section =========-->

  <!--=========== section =========-->
  {{-- @php
    $new = About::find(1);
  @endphp --}}
  <section>
<div class="container">
  <div class="row">
    <div class="contact">
  <div class="one">
  <h5>Join Our Team!</h5>
         <p>Our ongoing success depends on our people! We employ enthusiastic, energetic and effective people and we all believe in our vision, values and culture. We offer employees exciting and rewarding career choices. Please fill the form presented below if you believe you have what it takes to join our team!</p>
     </div>

     <div class="col-md-8 offset-md-3">

      <div class="form-group col-md-8 col-sm-8">
          <input type="text" class="form-control input-sm" id="name" placeholder="First Name">
          </div>

    <div class="form-group col-md-8 col-sm-8">
          <input type="text" class="form-control input-sm" id="name" placeholder="Last Name">
          </div>

    <div class="form-group col-md-8 col-sm-8">
          <input type="text" class="form-control input-sm" id="name" placeholder="Email">
          </div>

    <div class="form-group col-md-8 col-sm-8">
          <input type="text" class="form-control input-sm" id="name" placeholder="Job Title">
          </div>

    <div class="form-group col-md-8 col-sm-8">
          <input type="text" class="form-control input-sm" id="name" placeholder="City">
          </div>

    <div class="form-group col-md-8 col-sm-8">
          <input type="text" class="form-control input-sm" id="name" placeholder="Cover Letter">
          </div>

    <div class="form-group col-md-8 col-sm-8">
          <input type="File" class="form-control input-sm" id="name" placeholder="Choose File">
          </div>

    <div class="form-group col-md-7 col-sm-7">
          <button type="submit" class="btn btn-default" id="default1">Send</button>
          </div>

     </div>

     </div>



    </div>
 </div>
 </section>

   <div class="clearfix"></div>
  <!--=========== end section =========-->

  <!--=========== Footer section =========-->
   @include('includes.footer')
   <div class="clearfix"></div>
  <!--=========== end footer section =========-->

   <script>
   $(document).ready(function() {
 // executes when HTML-Document is loaded and DOM is ready
// breakpoint and up
$(window).resize(function(){
	if ($(window).width() >= 980){
      // when you hover a toggle show its dropdown menu
      $(".navbar .dropdown-toggle").hover(function () {
         $(this).parent().toggleClass("show");
         $(this).parent().find(".dropdown-menu").toggleClass("show");
       });
        // hide the menu when the mouse leaves the dropdown
      $( ".navbar .dropdown-menu" ).mouseleave(function() {
        $(this).removeClass("show");
      });

	}
});
});
   </script>
<script src="{{config('app.url')}}/assets/js/jquery.js"></script>
<script src="{{config('app.url')}}/assets/js/bootstrap.min.js"></script>
</body>

<!-- Mirrored from locksternsolutions.com/broccoli/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Feb 2019 17:15:04 GMT -->
</html>
