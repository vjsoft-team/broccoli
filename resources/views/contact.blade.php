{{-- @php
  use App\About;
@endphp --}}

<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from locksternsolutions.com/broccoli/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Feb 2019 17:11:18 GMT -->
<head>
<title>Broccoli - Contact us</title>
  <meta charset="UTF-8">
  <meta name="keywords" content="HTML,CSS,XML,JavaScript">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Site Icons -->
  <link href="{{config('app.url')}}/assets/img/icon.jpg" type="{{config('app.url')}}/assets/img/Home-512.png" rel="icon">

  <!-- font-icon -->
  <link rel="stylesheet" href="{{config('app.url')}}/assets/font-awesome/css/font-awesome.min.css">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/assets/css/bootstrap.min.css">

  <!-- Custom CSS -->
  <link rel="stylesheet" href="{{config('app.url')}}/assets/style.css">
  <link href="https://fonts.googleapis.com/css?family=Josefin+Sans|PT+Sans" rel="stylesheet">
  <!-- <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet"> -->
  <style>
  /* Always set the map height explicitly to define the size of the div
   * element that contains the map. */
  #map {
    height: 400px;
  }
  /* Optional: Makes the sample page fill the window. */
  /* html, body {
    height: 100%;
    margin: 0;
    padding: 0;
  } */
</style>


</head>
<body>
<!--=========== top head =========-->


<!--=========== end top head =========-->

<!--=========== Navbar section =========-->


 <!--=========== end brand section =========-->

  <!--=========== new section =========-->
  @include('includes.header')
  <!--=========== end new section =========-->

 <!--=========== Slider section =========-->



  <div class="clearfix"></div>
  <!--=========== end Slider section =========-->

  <!--=========== section =========-->
  {{-- @php
    $new = About::find(1);
  @endphp --}}
  <section>
	<div class="container">
	  <div class="row">
	  <!--============= text section ==============-->
	     <div class="contact-page">
		  <h6>
		  We're the friendliest pasta and pizza makers you'll ever meet.<br>
         call us, or shoot us an email. We'd love to get to know you.
		  </h6>

	    <div class="row">
		  <div class="col-md-4">
		    <div class="pizza">
			 <p>Broccoli Plzza & Pasta Yas lsland:</p>
			 <span>02 563 4003</span>
			</div>
		  </div>

		  <div class="col-md-4">
		    <div class="pizza">
			 <p>Broccoli Plzza & Pasta Yas lsland:</p>
			 <span>02 563 4003</span>
			</div>
		  </div>

		  <div class="col-md-4">
		    <div class="pizza">
			 <p>Broccoli Plzza & Pasta Yas lsland:</p>
			 <span>02 563 4003</span>
			</div>
		  </div>
		  </div>

		  <div class="row">
		  <div class="col-md-4">
		    <div class="pizza">
			 <p>Broccoli Plzza & Pasta Yas lsland:</p>
			 <span>02 563 4003</span>
			</div>
		  </div>

		  <div class="col-md-4">
		    <div class="pizza">
			 <p>Broccoli Plzza & Pasta Yas lsland:</p>
			 <span>02 563 4003</span>
			</div>
		  </div>

		  <div class="col-md-4">
		    <div class="pizza">
			 <p>Broccoli Plzza & Pasta Yas lsland:</p>
			 <span>02 563 4003</span>
			</div>
		  </div>
		  </div>


		  <div class="row">
		  <div class="col-md-4">
		    <div class="pizza">
			 <p>Broccoli Plzza & Pasta Yas lsland:</p>
			 <span>02 563 4003</span>
			</div>
		  </div>

		  <div class="col-md-4">
		    <div class="pizza">
			 <p>Broccoli Plzza & Pasta Yas lsland:</p>
			 <span>02 563 4003</span>
			</div>
		  </div>

		  <div class="col-md-4">
		    <div class="pizza">
			 <p>Broccoli Plzza & Pasta Yas lsland:</p>
			 <span>02 563 4003</span>
			</div>
		  </div>
		  </div>

		  <p class="four">Email : broccoli@broccoli.ae</p>
		 </div>
	<!--============= text section ==============-->

	<!--============= map section ==============-->

	<!--============= end map section ==============-->

	<!--============= form section ==============-->
	     <div class="contact-form">
		   <h4>Contact Us</h4>
		   <form action="php">
		   <div class="row">
		     <div class="form-group col-md-6 col-sm-6">
             <label for="name">First Name :</label>
             <input type="text" class="form-control input-sm" id="name" placeholder="">
             </div>

			 <div class="form-group col-md-6 col-sm-6">
             <label for="name">Last Name :</label>
             <input type="text" class="form-control input-sm" id="name" placeholder="">
             </div>

			 <div class="form-group col-md-6 col-sm-6">
             <label for="name">Email :</label>
             <input type="text" class="form-control input-sm" id="name" placeholder="">
             </div>

			 <div class="form-group col-md-6 col-sm-6">
             <label for="name">Phone :</label>
             <input type="text" class="form-control input-sm" id="name" placeholder="">
             </div>

			 <div class="form-group col-md-6 col-sm-6">
             <label for="name">Message :</label>
             	<textarea placeholder="Enter Address Here.." rows="2" class="form-control"></textarea>
             </div>

			 <div class="form-group col-md-6 col-sm-6" style="text-align:center;">
			  <br>
             <button type="submit" class="btn btn-default" id="default1">Submit</button>
             </div>
			 </form>
		   </div>
		 </div>
	<!--============= end form section ==============-->
  <div class="col-md-12" style="padding:0 0px;">
    {{-- <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3700370.3843409605!2d55.152408!3d25.080088!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x119f5c28d95b1658!2sBroccoli+Pizza+%26+Pasta!5e0!3m2!1sen!2sae!4v1551343488745" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe> --}}

  <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3700370.3843409605!2d55.152408!3d25.080088!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x119f5c28d95b1658!2sBroccoli+Pizza+%26+Pasta!5e0!3m2!1sen!2sae!4v1551343488745" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe> -->
  <div id="map"></div>
  </div>
  <div class="col-md-12">
    <br>
  </div>
      </div>
   </div>
   </section>


   <div class="clearfix"></div>
  <!--=========== end section =========-->

  <!--=========== Footer section =========-->
   @include('includes.footer')
   <div class="clearfix"></div>
  <!--=========== end footer section =========-->

   <script>
   $(document).ready(function() {
 // executes when HTML-Document is loaded and DOM is ready
// breakpoint and up
$(window).resize(function(){
	if ($(window).width() >= 980){
      // when you hover a toggle show its dropdown menu
      $(".navbar .dropdown-toggle").hover(function () {
         $(this).parent().toggleClass("show");
         $(this).parent().find(".dropdown-menu").toggleClass("show");
       });
        // hide the menu when the mouse leaves the dropdown
      $( ".navbar .dropdown-menu" ).mouseleave(function() {
        $(this).removeClass("show");
      });

	}
});
});
   </script>
<script src="{{config('app.url')}}/assets/js/jquery.js"></script>
<script src="{{config('app.url')}}/assets/js/bootstrap.min.js"></script>
<script>
  function initMap() {


  var locations = [
  ['<strong>Broccoli Pizza & Pasta</strong><br>Barsha Heights (Tecom), Al Hawai Residence - Dubai - United Arab Emirates Dubai United Arab Emirates' ,25.098208, 55.175767, 0],
  ['<strong>Broccoli Pizza & Pasta</strong><br>Va New Complex Dubai United Arab Emirates' ,25.231590, 55.262820, 1],
  ['<strong>Broccoli Pizza & Pasta</strong><br>Shop No. 1, Building No. 856,Sheikh Zayed The First St, Khalidiyah Abu Dhabi United Arab Emirates' ,24.476813, 54.351221, 2],
  ['<strong>Broccoli Pizza & Pasta</strong><br>Adnoc Petrol Station Yas Island أبو ظبي‎ United Arab Emirates' ,24.499493, 54.588370, 3],
  ['<strong>Broccoli Pizza & Pasta</strong><br>Al Nahyan, Al Mamoura Area, Muroor Road Abu Dhabi United Arab Emirates' ,24.464337, 54.386217, 4],
  ['<strong>Broccoli Pizza & Pasta</strong><br>ADNOC Petrol Station, Al Bahia North Abu Dhabi United Arab Emirates' ,24.542207, 54.636967, 5],
  ['<strong>Broccoli Pizza & Pasta</strong><br>666 Adnoc Station Abu Dhabi United Arab Emirates', 24.741403, 54.825385, 6],
  ['<strong>Broccoli Pizza & Pasta</strong><br>Prince Muhammad Ibn Saad Ibn Abdulaziz Rd, Al Malqa Riyadh 13524 Saudi Arabia', 24.803865, 46.604797, 7],
  ['<strong>Broccoli Pizza & Pasta</strong><br>النزهة - طريق عثمان بن عفان‎ الرياض‎ Saudi Arabia', 24.757759, 46.717714, 8],
  ];

  var map = new google.maps.Map(document.getElementById('map'), {
  zoom: 8,
  center: new google.maps.LatLng(25.098208, 55.175767),
  mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var infowindow = new google.maps.InfoWindow({});

  var marker, i;

  for (i = 0; i < locations.length; i++) {
  marker = new google.maps.Marker({
  position: new google.maps.LatLng(locations[i][1], locations[i][2]),
  map: map
  });

  google.maps.event.addListener(marker, 'click', (function (marker, i) {
  return function () {
    infowindow.setContent(locations[i][0]);
    infowindow.open(map, marker);
  }
  })(marker, i));
  }
  }
      </script>
  <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCCOQlODnt4FYLKx9k4ppdHA7O7tKBoa18&callback=initMap">
      </script>

</body>

<!-- Mirrored from locksternsolutions.com/broccoli/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Feb 2019 17:15:04 GMT -->
</html>
