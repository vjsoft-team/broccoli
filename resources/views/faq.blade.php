{{-- @php
  use App\About;
@endphp --}}

<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from locksternsolutions.com/broccoli/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Feb 2019 17:11:18 GMT -->
<head>
<title>Broccoli - FAQ</title>
  <meta charset="UTF-8">
  <meta name="keywords" content="HTML,CSS,XML,JavaScript">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Site Icons -->
  <link href="{{config('app.url')}}/assets/img/icon.jpg" type="{{config('app.url')}}/assets/img/Home-512.png" rel="icon">

  <!-- font-icon -->
  <link rel="stylesheet" href="{{config('app.url')}}/assets/font-awesome/css/font-awesome.min.css">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/assets/css/bootstrap.min.css">

  <!-- Custom CSS -->
  <link rel="stylesheet" href="{{config('app.url')}}/assets/style.css">
  <link href="https://fonts.googleapis.com/css?family=Josefin+Sans|PT+Sans" rel="stylesheet">
  <!-- <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet"> -->



</head>
<body>
<!--=========== top head =========-->


<!--=========== end top head =========-->

<!--=========== Navbar section =========-->


 <!--=========== end brand section =========-->

  <!--=========== new section =========-->
  @include('includes.header')
  <!--=========== end new section =========-->

 <!--=========== Slider section =========-->



  <div class="clearfix"></div>
  <!--=========== end Slider section =========-->

  <!--=========== section =========-->
  {{-- @php
    $new = About::find(1);
  @endphp --}}
  <section class="faq">
<p><br><p>
<div class="container">
     <div class="row">
   <h6>FAQ</h6>
     <div id="accordion">

        <div class="card">
        <div class="card-header" data-toggle="collapse" href="#collapseOne">
        <h4 class="card-link">
        <i class="fa fa-angle-double-right"></i> Q. Can I reserve a table at a Broccoli Pizza & Pasta Restaurant?
        </h4>
       </div>
       <div id="collapseOne" class="collapse show" data-parent="#accordion">
       <div class="card-body">
         No, because at Broccoli Pizza & Pasta it is always “First-come, First-served”.
       </div>
       </div>
       </div>

       <div class="card">
       <div class="card-header" data-toggle="collapse" href="#collapseTwo">
       <h4 class="card-link">
       <i class="fa fa-angle-double-right"></i> Q. How do I contact Broccoli Pizza & Pasta?
       </h4>
       </div>
       <div id="collapseTwo" class="collapse" data-parent="#accordion">
       <div class="card-body">
        We are great listeners and would be delighted to hear from you. You can either use our online form, or email us on broccoli@broccoli.ae
       </div>
       </div>
       </div>


   <div class="card">
       <div class="card-header" data-toggle="collapse" href="#collapse3">
       <h4 class="card-link">
        <i class="fa fa-angle-double-right"></i> Q. Where can I find out about the latest Broccoli Pizza & Pasta promotions?
       </h4>
       </div>
       <div id="collapse3" class="collapse" data-parent="#accordion">
       <div class="card-body">
         It’s easy! All the appetizing details of our latest special offers are on our website. Simply click here!
       </div>
       </div>
       </div>

   <div class="card">
       <div class="card-header" data-toggle="collapse" href="#collapse4">
       <h4 class="card-link">
        <i class="fa fa-angle-double-right"></i> Q. Are you involved in any program to help schools?
        </h4>
       </div>
       <div id="collapse4" class="collapse" data-parent="#accordion">
       <div class="card-body">
         Definitely. While our tongue-teasing food feeds the body, education gives children “food for thought” so we passionately sponsor events at schools. To learn more, For more contact the Manager at your nearest Broccoli Pizza & Pasta Restaurant
       </div>
       </div>
       </div>


   <div class="card">
       <div class="card-header" data-toggle="collapse" href="#collapse5">
       <h4 class="card-link">
        <i class="fa fa-angle-double-right"></i> Q. What information do you have regarding allergens or the suitability of the menu for different diets?
        </h4>
       </div>
       <div id="collapse5" class="collapse" data-parent="#accordion">
       <div class="card-body">
         It will be our pleasure to help you. Simply email us at broccoli@broccoli.ae
       </div>
       </div>
       </div>
   <div class="card">
       <div class="card-header" data-toggle="collapse" href="#collapse6">
       <h4 class="card-link">
        <i class="fa fa-angle-double-right"></i> Q. Can I contact you to find out more about whether your food is suitable for my personal or religious tastes?
        </h4>
       </div>
       <div id="collapse6" class="collapse" data-parent="#accordion">
       <div class="card-body">
         All the food that Broccoli Pizza & Pasta serve is halal, and we use the highest quality ingredients. Please email us for futher information at broccoli@broccoli.ae
       </div>
       </div>
       </div>
   <div class="card">
       <div class="card-header" data-toggle="collapse" href="#collapse7">
       <h4 class="card-link">
        <i class="fa fa-angle-double-right"></i> Q. Do you closely follow the Government regulations?
        </h4>
       </div>
       <div id="collapse7" class="collapse" data-parent="#accordion">
       <div class="card-body">
         All the food that Broccoli Pizza & Pasta serve is halal, and we use the highest quality ingredients. Please email us for futher information at broccoli@broccoli.ae
       </div>
       </div>
       </div>
   <div class="card">
       <div class="card-header" data-toggle="collapse" href="#collapse8">
       <h4 class="card-link">
        <i class="fa fa-angle-double-right"></i> Q. I am a food and beverages supplier, who do I contact to discuss my products?
        </h4>
       </div>
       <div id="collapse8" class="collapse" data-parent="#accordion">
       <div class="card-body">
         Broccoli Pizza & Pasta uses only the finest ingredients and we have approved suppliers all over the world. To introduce your company please contact broccoli@broccoli.ae. We will contact you to discuss further should we have a need for your product.
       </div>
       </div>
       </div>
   <div class="card">
       <div class="card-header" data-toggle="collapse" href="#collapse9">
       <h4 class="card-link">
        <i class="fa fa-angle-double-right"></i> Q. Does Broccoli Pizza & Pasta offer take-out and delivery services?
        </h4>
       </div>
       <div id="collapse9" class="collapse" data-parent="#accordion">
       <div class="card-body">
         Yes, of course we do depending on the restaurant and location, please contact the closest restaurant to you to find out more.
       </div>
       </div>
       </div>
   <div class="card">
       <div class="card-header" data-toggle="collapse" href="#collapse10">
       <h4 class="card-link">
        <i class="fa fa-angle-double-right"></i> Q. Do you have any nutritional information?
        </h4>
       </div>
       <div id="collapse10" class="collapse" data-parent="#accordion">
       <div class="card-body">
         Yes we have detailed nutritional information, and we can provide it to you at your request. Please email us at broccoli@broccoli.ae
       </div>
       </div>
       </div>
   <div class="card">
       <div class="card-header" data-toggle="collapse" href="#collapse11">
       <h4 class="card-link">
        <i class="fa fa-angle-double-right"></i> Q. Do you cater for events?
        </h4>
       </div>
       <div id="collapse11" class="collapse" data-parent="#accordion">
       <div class="card-body">
         Yes we do, however we need advance notice. Please contact the individual restaurants and give them your requirements. Otherwise contact broccoli@broccoli.ae
       </div>
       </div>
       </div>
   <div class="card">
       <div class="card-header" data-toggle="collapse" href="#collapse12">
       <h4 class="card-link">
        <i class="fa fa-angle-double-right"></i> Q. Does Broccoli Pizza & Pasta have a guest loyalty program?
        </h4>
       </div>
       <div id="collapse12" class="collapse" data-parent="#accordion">
       <div class="card-body">
         The initial term of the Franchise Agreement is five years from the date the Franchise Agreement is signed. The franchise is renewable for four additional terms of five years each, providing you have complied with the provisions of the Franchise Agreement.
       </div>
       </div>
       </div>
   <div class="card">
       <div class="card-header" data-toggle="collapse" href="#collapse13">
       <h4 class="card-link">
        <i class="fa fa-angle-double-right"></i> Q. How can I apply for a job at Broccoli Pizza & Pasta?
        </h4>
       </div>
       <div id="collapse13" class="collapse" data-parent="#accordion">
       <div class="card-body">
         We are always looking for talented staff both in our restaurants  across the globe and our head office, Please contact recruitment@broccoli.ae
       </div>
       </div>
       </div>
   <div class="card">
       <div class="card-header" data-toggle="collapse" href="#collapse14">
       <h4 class="card-link">
        <i class="fa fa-angle-double-right"></i> Q. Where do I send information on marketing and advertising opportunities?
        </h4>
       </div>
       <div id="collapse14" class="collapse" data-parent="#accordion">
       <div class="card-body">
         Our marketing department deals with all enquiries, please email marketing@broccoli.ae
       </div>
       </div>
       </div>
   <div class="card">
       <div class="card-header" data-toggle="collapse" href="#collapse15">
       <h4 class="card-link">
        <i class="fa fa-angle-double-right"></i> Q. Is the Broccoli Pizza & Pasta menu suitable for children?
        </h4>
       </div>
       <div id="collapse15" class="collapse" data-parent="#accordion">
       <div class="card-body">
         Our food is healthy, nutritious, and can be eaten as part of a balanced diet. The menu has been specifically designed in mind to cater to childrens tastes in food. Should you have any specific requirements for your child please ask the staff in our restaurants, and we will try to accommodate you.
       </div>
       </div>
       </div>
   <div class="card">
       <div class="card-header" data-toggle="collapse" href="#collapse16">
       <h4 class="card-link">
        <i class="fa fa-angle-double-right"></i> Q. Do you use GM (Genetically Modified) products?
        </h4>
       </div>
       <div id="collapse16" class="collapse" data-parent="#accordion">
       <div class="card-body">
         All our food is fresh, and we do not serve any food that has been genetically modified.
       </div>
       </div>
       </div>
   <div class="card">
       <div class="card-header" data-toggle="collapse" href="#collapse17">
       <h4 class="card-link">
        <i class="fa fa-angle-double-right"></i> Q. Where can I share feedback on my Broccoli Pizza & Pasta dining experience?
        </h4>
       </div>
       <div id="collapse17" class="collapse" data-parent="#accordion">
       <div class="card-body">
         In all of our restaurants there is a feedback form's, we would encourage you to fill this out, otherwise please email broccoli@broccoli.ae
       </div>
       </div>
       </div>


</div>
   </div>
</div>
<p><br></p>
 </section>


   <div class="clearfix"></div>
  <!--=========== end section =========-->

  <!--=========== Footer section =========-->
   @include('includes.footer')
   <div class="clearfix"></div>
  <!--=========== end footer section =========-->

   <script>
   $(document).ready(function() {
 // executes when HTML-Document is loaded and DOM is ready
// breakpoint and up
$(window).resize(function(){
	if ($(window).width() >= 980){
      // when you hover a toggle show its dropdown menu
      $(".navbar .dropdown-toggle").hover(function () {
         $(this).parent().toggleClass("show");
         $(this).parent().find(".dropdown-menu").toggleClass("show");
       });
        // hide the menu when the mouse leaves the dropdown
      $( ".navbar .dropdown-menu" ).mouseleave(function() {
        $(this).removeClass("show");
      });

	}
});
});
   </script>
<script src="{{config('app.url')}}/assets/js/jquery.js"></script>
<script src="{{config('app.url')}}/assets/js/bootstrap.min.js"></script>
{{-- <script>
  function initMap() {


  var locations = [
  ['<strong>Broccoli Pizza & Pasta</strong><br>Barsha Heights (Tecom), Al Hawai Residence - Dubai - United Arab Emirates Dubai United Arab Emirates' ,25.098208, 55.175767, 0],
  ['<strong>Broccoli Pizza & Pasta</strong><br>Va New Complex Dubai United Arab Emirates' ,25.231590, 55.262820, 1],
  ['<strong>Broccoli Pizza & Pasta</strong><br>Shop No. 1, Building No. 856,Sheikh Zayed The First St, Khalidiyah Abu Dhabi United Arab Emirates' ,24.476813, 54.351221, 2],
  ['<strong>Broccoli Pizza & Pasta</strong><br>Adnoc Petrol Station Yas Island أبو ظبي‎ United Arab Emirates' ,24.499493, 54.588370, 3],
  ['<strong>Broccoli Pizza & Pasta</strong><br>Al Nahyan, Al Mamoura Area, Muroor Road Abu Dhabi United Arab Emirates' ,24.464337, 54.386217, 4],
  ['<strong>Broccoli Pizza & Pasta</strong><br>ADNOC Petrol Station, Al Bahia North Abu Dhabi United Arab Emirates' ,24.542207, 54.636967, 5],
  ['<strong>Broccoli Pizza & Pasta</strong><br>666 Adnoc Station Abu Dhabi United Arab Emirates', 24.741403, 54.825385, 6],
  ['<strong>Broccoli Pizza & Pasta</strong><br>Prince Muhammad Ibn Saad Ibn Abdulaziz Rd, Al Malqa Riyadh 13524 Saudi Arabia', 24.803865, 46.604797, 7],
  ['<strong>Broccoli Pizza & Pasta</strong><br>النزهة - طريق عثمان بن عفان‎ الرياض‎ Saudi Arabia', 24.757759, 46.717714, 8],
  ];

  var map = new google.maps.Map(document.getElementById('map'), {
  zoom: 8,
  center: new google.maps.LatLng(25.098208, 55.175767),
  mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var infowindow = new google.maps.InfoWindow({});

  var marker, i;

  for (i = 0; i < locations.length; i++) {
  marker = new google.maps.Marker({
  position: new google.maps.LatLng(locations[i][1], locations[i][2]),
  map: map
  });

  google.maps.event.addListener(marker, 'click', (function (marker, i) {
  return function () {
    infowindow.setContent(locations[i][0]);
    infowindow.open(map, marker);
  }
  })(marker, i));
  }
  }
      </script> --}}
  {{-- <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCCOQlODnt4FYLKx9k4ppdHA7O7tKBoa18&callback=initMap">
      </script> --}}

</body>

<!-- Mirrored from locksternsolutions.com/broccoli/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Feb 2019 17:15:04 GMT -->
</html>
