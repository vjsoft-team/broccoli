@php
  use App\Location;
  $new = Location::all();
@endphp

<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from locksternsolutions.com/broccoli/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Feb 2019 17:11:18 GMT -->
<head>
<title>Broccoli - Location</title>
  <meta charset="UTF-8">
  <meta name="keywords" content="HTML,CSS,XML,JavaScript">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Site Icons -->
  <link href="{{config('app.url')}}/assets/img/icon.jpg" type="{{config('app.url')}}/assets/img/Home-512.png" rel="icon">

  <!-- font-icon -->
  <link rel="stylesheet" href="{{config('app.url')}}/assets/font-awesome/css/font-awesome.min.css">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/assets/css/bootstrap.min.css">

  <!-- Custom CSS -->
  <link rel="stylesheet" href="{{config('app.url')}}/assets/style.css">
  <link href="https://fonts.googleapis.com/css?family=Josefin+Sans|PT+Sans" rel="stylesheet">

  <link href="{{config('app.url')}}/assets/cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <!-- <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet"> -->
  <style>
  /* Always set the map height explicitly to define the size of the div
   * element that contains the map. */
  #map {
    height: 400px;
  }
  /* Optional: Makes the sample page fill the window. */
  /* html, body {
    height: 100%;
    margin: 0;
    padding: 0;
  } */
</style>


</head>
<body>
<!--=========== top head =========-->


<!--=========== end top head =========-->

<!--=========== Navbar section =========-->


 <!--=========== end brand section =========-->

  <!--=========== new section =========-->
  @include('includes.header')
  <!--=========== end new section =========-->

 <!--=========== Slider section =========-->



  <div class="clearfix"></div>
  <!--=========== end Slider section =========-->

  <!--=========== section =========-->
  {{-- @php
    $new = About::find(1);
  @endphp --}}
  <section class="location">
  <div class="container">
    <div class="row">
    <div class="location1">
      <h3>Find a Restaurant</h3>
    <p>Find our nearest restaurant by giving your Loaction</p>
      </div>
  </div>

  <div class="row">
    <div class="location2">
    <h5>Enter Your Location</h5>
    </div>
   </div>

   <div class="row">
    <div class="form-group col-md-3 col-sm-3">
           <label for="name">Country/City :</label>
           <!-- <input type="text" class="form-control input-sm" id="name" placeholder="" required> -->
           <select class="js-example-basic-single form-control input-sm" id="name" name="state">
            <option value="">Select</option>
            <option value="AL">Abu Dhabi</option>
            <option value="WY">Saudi Arabia</option>
          </select>
       </div>

   <div class="form-group col-md-3 col-sm-3">
           <label for="name">Location :</label>
           <!-- <input type="text" class="form-control input-sm" id="name" placeholder="" required> -->
           <select class="js-example-basic-single form-control input-sm" id="location" name="state">
             <option value="">Select</option>
            <option value="AL">Location 1</option>
            <option value="WY">Location 2</option>
          </select>
       </div>

   <div class="form-group col-md-3 col-sm-3">
           <label for="name">Area Name :</label>
           <!-- <input type="text" class="form-control input-sm" id="name" placeholder="" required> -->
           <select class="js-example-basic-single form-control input-sm" id="area" name="state">
             <option value="">Select</option>
            <option value="AL">Area 1</option>
            <option value="WY">Area 2</option>
          </select>
       </div>

    <div class="form-group col-md-3 col-sm-3"></div>

   <div class="form-group col-md-2 col-sm-2">
   <button type="button" class="btn btn-default" id="location">CONTINUE</button>
   </div>
  </div>

  <div class="row" id="location1">
     <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d15284579.905983415!2d57.494658282449485!3d20.72699342516921!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e6!4m5!1s0x3bcb9762d0094f87%3A0xdf25b94fca10f0e3!2sPizza+Zone%2C+4-1-548%2F1%2C+Shop+No.6%2C+Needs+Arcade+Complex%2C+Boggul+Kunta+Cross+Roads%2C+Abids%2C+Hyderabad%2C+Telangana+500001!3m2!1d17.389201399999997!2d78.4806854!4m5!1s0x3e5e4540629d0dc1%3A0x4ae17fbbc92e219!2sBroccoli+Pizza+%26+Pasta%2C+Adnoc+Petrol+Station+Yas+Island+-+Abu+Dhabi+-+United+Arab+Emirates!3m2!1d24.4993613!2d54.588369799999995!5e0!3m2!1sen!2sin!4v1550820926128" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe> -->
  </div>
  <div id="map"></div>

   <p><br></p>
  <div class="row">
    @foreach ($new as $key)
     <div class="col-md-3 location0">
     <div class="location4">
       <h6>{{$key->location}}-{{$key->country}}</h6>
       <p>Tel:{{$key->telephone}} Call{{$key->mobile}}</p>
       <span>Hours:</span>
       <h3>Carryout:{{$key->carry_out}}</h3>
       <h3>Delivery:{{$key->delivery}}</h3>
       <p>{{$key->description}}<p>

         <p>{{$key->mobile}} for phone Ortders</p>
     </div>
     </div>
   @endforeach

     {{-- <div class="col-md-3 location0">
     <div class="location4">
       <h6>Barari Mall-UAE</h6>
       <p>Tel:03.78.40735 Call60.05.20.00.1</p>
       <span>Hours:</span>
       <h3>Carryout: 10:30 am-11:59 pm</h3>
       <h3>Delivery: 10:30 am-11:29 pm</h3>
       <p><p>
       <p>Order Inline 24 hours a day</p>
       <p>with respect to store timings.</p>
       <p>Dial to our call center Number</p>
         <p>60.05.20.00.1 for phone Ortders</p>
     </div>
     </div> --}}

     {{-- <div class="col-md-3 location0">
     <div class="location4">
       <h6>Barari Mall-UAE</h6>
       <p>Tel:03.78.40735 Call60.05.20.00.1</p>
       <span>Hours:</span>
       <h3>Carryout: 10:30 am-11:59 pm</h3>
       <h3>Delivery: 10:30 am-11:29 pm</h3>
       <p><p>
       <p>Order Inline 24 hours a day</p>
       <p>with respect to store timings.</p>
       <p>Dial to our call center Number</p>
         <p>60.05.20.00.1 for phone Ortders</p>
     </div>
     </div> --}}

     {{-- <div class="col-md-3 location0">
     <div class="location4">
       <h6>Barari Mall-UAE</h6>
       <p>Tel:03.78.40735 Call60.05.20.00.1</p>
       <span>Hours:</span>
       <h3>Carryout: 10:30 am-11:59 pm</h3>
       <h3>Delivery: 10:30 am-11:29 pm</h3>
       <p><p>
       <p>Order Inline 24 hours a day</p>
       <p>with respect to store timings.</p>
       <p>Dial to our call center Number</p>
         <p>60.05.20.00.1 for phone Ortders</p>
     </div>
     </div> --}}

  </div>

  {{-- <div class="row"> --}}
     {{-- <div class="col-md-3 location0">
     <div class="location4">
       <h6>Barari Mall-UAE</h6>
       <p>Tel:03.78.40735 Call60.05.20.00.1</p>
       <span>Hours:</span>
       <h3>Carryout: 10:30 am-11:59 pm</h3>
       <h3>Delivery: 10:30 am-11:29 pm</h3>
       <p><p>
       <p>Order Inline 24 hours a day</p>
       <p>with respect to store timings.</p>
       <p>Dial to our call center Number</p>
         <p>60.05.20.00.1 for phone Ortders</p>
     </div>
     </div> --}}

     {{-- <div class="col-md-3 location0">
     <div class="location4">
       <h6>Barari Mall-UAE</h6>
       <p>Tel:03.78.40735 Call60.05.20.00.1</p>
       <span>Hours:</span>
       <h3>Carryout: 10:30 am-11:59 pm</h3>
       <h3>Delivery: 10:30 am-11:29 pm</h3>
       <p><p>
       <p>Order Inline 24 hours a day</p>
       <p>with respect to store timings.</p>
       <p>Dial to our call center Number</p>
         <p>60.05.20.00.1 for phone Ortders</p>
     </div>
     </div> --}}

     {{-- <div class="col-md-3 location0">
     <div class="location4">
       <h6>Barari Mall-UAE</h6>
       <p>Tel:03.78.40735 Call60.05.20.00.1</p>
       <span>Hours:</span>
       <h3>Carryout: 10:30 am-11:59 pm</h3>
       <h3>Delivery: 10:30 am-11:29 pm</h3>
       <p><p>
       <p>Order Inline 24 hours a day</p>
       <p>with respect to store timings.</p>
       <p>Dial to our call center Number</p>
         <p>60.05.20.00.1 for phone Ortders</p>
     </div>
     </div> --}}

     {{-- <div class="col-md-3 location0">
     <div class="location4">
       <h6>Barari Mall-UAE</h6>
       <p>Tel:03.78.40735 Call60.05.20.00.1</p>
       <span>Hours:</span>
       <h3>Carryout: 10:30 am-11:59 pm</h3>
       <h3>Delivery: 10:30 am-11:29 pm</h3>
       <p><p>
       <p>Order Inline 24 hours a day</p>
       <p>with respect to store timings.</p>
       <p>Dial to our call center Number</p>
         <p>60.05.20.00.1 for phone Ortders</p>
     </div>
     </div> --}}
  {{-- </div> --}}

  {{-- <div class="row">
     <div class="col-md-3 location0">
     <div class="location4">
       <h6>Barari Mall-UAE</h6>
       <p>Tel:03.78.40735 Call60.05.20.00.1</p>
       <span>Hours:</span>
       <h3>Carryout: 10:30 am-11:59 pm</h3>
       <h3>Delivery: 10:30 am-11:29 pm</h3>
       <p><p>
       <p>Order Inline 24 hours a day</p>
       <p>with respect to store timings.</p>
       <p>Dial to our call center Number</p>
         <p>60.05.20.00.1 for phone Ortders</p>
     </div>
     </div>

     <div class="col-md-3 location0">
     <div class="location4">
       <h6>Barari Mall-UAE</h6>
       <p>Tel:03.78.40735 Call60.05.20.00.1</p>
       <span>Hours:</span>
       <h3>Carryout: 10:30 am-11:59 pm</h3>
       <h3>Delivery: 10:30 am-11:29 pm</h3>
       <p><p>
       <p>Order Inline 24 hours a day</p>
       <p>with respect to store timings.</p>
       <p>Dial to our call center Number</p>
         <p>60.05.20.00.1 for phone Ortders</p>
     </div>
     </div>

     <div class="col-md-3 location0">
     <div class="location4">
       <h6>Barari Mall-UAE</h6>
       <p>Tel:03.78.40735 Call60.05.20.00.1</p>
       <span>Hours:</span>
       <h3>Carryout: 10:30 am-11:59 pm</h3>
       <h3>Delivery: 10:30 am-11:29 pm</h3>
       <p><p>
       <p>Order Inline 24 hours a day</p>
       <p>with respect to store timings.</p>
       <p>Dial to our call center Number</p>
         <p>60.05.20.00.1 for phone Ortders</p>
     </div>
     </div>

     <div class="col-md-3 location0">
     <div class="location4">
       <h6>Barari Mall-UAE</h6>
       <p>Tel:03.78.40735 Call60.05.20.00.1</p>
       <span>Hours:</span>
       <h3>Carryout: 10:30 am-11:59 pm</h3>
       <h3>Delivery: 10:30 am-11:29 pm</h3>
       <p><p>
       <p>Order Inline 24 hours a day</p>
       <p>with respect to store timings.</p>
       <p>Dial to our call center Number</p>
         <p>60.05.20.00.1 for phone Ortders</p>
     </div>
     </div>
  </div> --}}

  {{-- <div class="row">
     <div class="col-md-3 location0">
     <div class="location4">
       <h6>Barari Mall-UAE</h6>
       <p>Tel:03.78.40735 Call60.05.20.00.1</p>
       <span>Hours:</span>
       <h3>Carryout: 10:30 am-11:59 pm</h3>
       <h3>Delivery: 10:30 am-11:29 pm</h3>
       <p><p>
       <p>Order Inline 24 hours a day</p>
       <p>with respect to store timings.</p>
       <p>Dial to our call center Number</p>
         <p>60.05.20.00.1 for phone Ortders</p>
     </div>
     </div>

     <div class="col-md-3 location0">
     <div class="location4">
       <h6>Barari Mall-UAE</h6>
       <p>Tel:03.78.40735 Call60.05.20.00.1</p>
       <span>Hours:</span>
       <h3>Carryout: 10:30 am-11:59 pm</h3>
       <h3>Delivery: 10:30 am-11:29 pm</h3>
       <p><p>
       <p>Order Inline 24 hours a day</p>
       <p>with respect to store timings.</p>
       <p>Dial to our call center Number</p>
         <p>60.05.20.00.1 for phone Ortders</p>
     </div>
     </div>

     <div class="col-md-3 location0">
     <div class="location4">
       <h6>Barari Mall-UAE</h6>
       <p>Tel:03.78.40735 Call60.05.20.00.1</p>
       <span>Hours:</span>
       <h3>Carryout: 10:30 am-11:59 pm</h3>
       <h3>Delivery: 10:30 am-11:29 pm</h3>
       <p><p>
       <p>Order Inline 24 hours a day</p>
       <p>with respect to store timings.</p>
       <p>Dial to our call center Number</p>
         <p>60.05.20.00.1 for phone Ortders</p>
     </div>
     </div>

     <div class="col-md-3 location0">
     <div class="location4">
       <h6>Barari Mall-UAE</h6>
       <p>Tel:03.78.40735 Call60.05.20.00.1</p>
       <span>Hours:</span>
       <h3>Carryout: 10:30 am-11:59 pm</h3>
       <h3>Delivery: 10:30 am-11:29 pm</h3>
       <p><p>
       <p>Order Inline 24 hours a day</p>
       <p>with respect to store timings.</p>
       <p>Dial to our call center Number</p>
         <p>60.05.20.00.1 for phone Ortders</p>
     </div>
     </div>

  </div> --}}

    </div>
 </section>


   <div class="clearfix"></div>
  <!--=========== end section =========-->

  <!--=========== Footer section =========-->
   @include('includes.footer')
   <div class="clearfix"></div>
  <!--=========== end footer section =========-->

   <script>
   $(document).ready(function() {
 // executes when HTML-Document is loaded and DOM is ready
// breakpoint and up
$(window).resize(function(){
	if ($(window).width() >= 980){
      // when you hover a toggle show its dropdown menu
      $(".navbar .dropdown-toggle").hover(function () {
         $(this).parent().toggleClass("show");
         $(this).parent().find(".dropdown-menu").toggleClass("show");
       });
        // hide the menu when the mouse leaves the dropdown
      $( ".navbar .dropdown-menu" ).mouseleave(function() {
        $(this).removeClass("show");
      });

	}
});
});
   </script>


   <script src="{{config('app.url')}}/assets/cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

   <script type="text/javascript">
   $(document).ready(function() {
     $('.js-example-basic-single').select2();
   });
   </script>


<script src="{{config('app.url')}}/assets/js/jquery.js"></script>
<script src="{{config('app.url')}}/assets/js/bootstrap.min.js"></script>
<script>
  function initMap() {


  var locations = [
  ['<strong>Broccoli Pizza & Pasta</strong><br>Barsha Heights (Tecom), Al Hawai Residence - Dubai - United Arab Emirates Dubai United Arab Emirates' ,25.098208, 55.175767, 0],
  ['<strong>Broccoli Pizza & Pasta</strong><br>Va New Complex Dubai United Arab Emirates' ,25.231590, 55.262820, 1],
  ['<strong>Broccoli Pizza & Pasta</strong><br>Shop No. 1, Building No. 856,Sheikh Zayed The First St, Khalidiyah Abu Dhabi United Arab Emirates' ,24.476813, 54.351221, 2],
  ['<strong>Broccoli Pizza & Pasta</strong><br>Adnoc Petrol Station Yas Island أبو ظبي‎ United Arab Emirates' ,24.499493, 54.588370, 3],
  ['<strong>Broccoli Pizza & Pasta</strong><br>Al Nahyan, Al Mamoura Area, Muroor Road Abu Dhabi United Arab Emirates' ,24.464337, 54.386217, 4],
  ['<strong>Broccoli Pizza & Pasta</strong><br>ADNOC Petrol Station, Al Bahia North Abu Dhabi United Arab Emirates' ,24.542207, 54.636967, 5],
  ['<strong>Broccoli Pizza & Pasta</strong><br>666 Adnoc Station Abu Dhabi United Arab Emirates', 24.741403, 54.825385, 6],
  ['<strong>Broccoli Pizza & Pasta</strong><br>Prince Muhammad Ibn Saad Ibn Abdulaziz Rd, Al Malqa Riyadh 13524 Saudi Arabia', 24.803865, 46.604797, 7],
  ['<strong>Broccoli Pizza & Pasta</strong><br>النزهة - طريق عثمان بن عفان‎ الرياض‎ Saudi Arabia', 24.757759, 46.717714, 8],
  ];

  var map = new google.maps.Map(document.getElementById('map'), {
  zoom: 8,
  center: new google.maps.LatLng(25.098208, 55.175767),
  mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var infowindow = new google.maps.InfoWindow({});

  var marker, i;

  for (i = 0; i < locations.length; i++) {
  marker = new google.maps.Marker({
  position: new google.maps.LatLng(locations[i][1], locations[i][2]),
  map: map
  });

  google.maps.event.addListener(marker, 'click', (function (marker, i) {
  return function () {
    infowindow.setContent(locations[i][0]);
    infowindow.open(map, marker);
  }
  })(marker, i));
  }
  }
      </script>
  <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCCOQlODnt4FYLKx9k4ppdHA7O7tKBoa18&callback=initMap">
      </script><script>
      $(document).ready(function() {
      // executes when HTML-Document is loaded and DOM is ready
      // breakpoint and up
      $(window).resize(function(){
      if ($(window).width() >= 980){
         // when you hover a toggle show its dropdown menu
         $(".navbar .dropdown-toggle").hover(function () {
            $(this).parent().toggleClass("show");
            $(this).parent().find(".dropdown-menu").toggleClass("show");
          });
           // hide the menu when the mouse leaves the dropdown
         $( ".navbar .dropdown-menu" ).mouseleave(function() {
           $(this).removeClass("show");
         });

      }
      });
      });
      </script>
      <script src="{{config('app.url')}}/assets/js/jquery.js"></script>
      <script src="{{config('app.url')}}/assets/js/bootstrap.min.js"></script>
      <script src="{{config('app.url')}}/assets/cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
      <script type="text/javascript">
      $(document).ready(function() {
        $('.js-example-basic-single').select2();
      });
      </script>
      <script>
      function initMap() {


      var locations = [
      ['<strong>Broccoli Pizza & Pasta</strong><br>Barsha Heights (Tecom), Al Hawai Residence - Dubai - United Arab Emirates Dubai United Arab Emirates' ,25.098208, 55.175767, 0],
      ['<strong>Broccoli Pizza & Pasta</strong><br>Va New Complex Dubai United Arab Emirates' ,25.231590, 55.262820, 1],
      ['<strong>Broccoli Pizza & Pasta</strong><br>Shop No. 1, Building No. 856,Sheikh Zayed The First St, Khalidiyah Abu Dhabi United Arab Emirates' ,24.476813, 54.351221, 2],
      ['<strong>Broccoli Pizza & Pasta</strong><br>Adnoc Petrol Station Yas Island أبو ظبي‎ United Arab Emirates' ,24.499493, 54.588370, 3],
      ['<strong>Broccoli Pizza & Pasta</strong><br>Al Nahyan, Al Mamoura Area, Muroor Road Abu Dhabi United Arab Emirates' ,24.464337, 54.386217, 4],
      ['<strong>Broccoli Pizza & Pasta</strong><br>ADNOC Petrol Station, Al Bahia North Abu Dhabi United Arab Emirates' ,24.542207, 54.636967, 5],
      ['<strong>Broccoli Pizza & Pasta</strong><br>666 Adnoc Station Abu Dhabi United Arab Emirates', 24.741403, 54.825385, 6],
      ['<strong>Broccoli Pizza & Pasta</strong><br>Prince Muhammad Ibn Saad Ibn Abdulaziz Rd, Al Malqa Riyadh 13524 Saudi Arabia', 24.803865, 46.604797, 7],
      ['<strong>Broccoli Pizza & Pasta</strong><br>النزهة - طريق عثمان بن عفان‎ الرياض‎ Saudi Arabia', 24.757759, 46.717714, 8],
      ];

      var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 8,
      center: new google.maps.LatLng(25.098208, 55.175767),
      mapTypeId: google.maps.MapTypeId.ROADMAP
      });

      var infowindow = new google.maps.InfoWindow({});

      var marker, i;

      for (i = 0; i < locations.length; i++) {
      marker = new google.maps.Marker({
      position: new google.maps.LatLng(locations[i][1], locations[i][2]),
      map: map
      });

      google.maps.event.addListener(marker, 'click', (function (marker, i) {
      return function () {
        infowindow.setContent(locations[i][0]);
        infowindow.open(map, marker);
      }
      })(marker, i));
      }
      }
          </script>
      <script async defer
          src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCCOQlODnt4FYLKx9k4ppdHA7O7tKBoa18&amp;callback=initMap">
          </script>

</body>

<!-- Mirrored from locksternsolutions.com/broccoli/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Feb 2019 17:15:04 GMT -->
</html>
