@php
  use App\Slider;
  use App\HomeGrid;
  $pictures = Slider::all();
@endphp

<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from locksternsolutions.com/broccoli/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Feb 2019 17:11:18 GMT -->
<head>
<title>Broccoli</title>
  <meta charset="UTF-8">
  <meta name="keywords" content="HTML,CSS,XML,JavaScript">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Site Icons -->
  <link href="{{config('app.url')}}/assets/img/icon.jpg" type="{{config('app.url')}}/assets/img/Home-512.png" rel="icon">

  <!-- font-icon -->
  <link rel="stylesheet" href="{{config('app.url')}}/assets/font-awesome/css/font-awesome.min.css">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/assets/css/bootstrap.min.css">

  <!-- Custom CSS -->
  <link rel="stylesheet" href="{{config('app.url')}}/assets/style.css">
  <link href="https://fonts.googleapis.com/css?family=Josefin+Sans|PT+Sans" rel="stylesheet">
  <!-- <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet"> -->

</head>
<body>
<!--=========== top head =========-->


<!--=========== end top head =========-->

<!--=========== Navbar section =========-->


 <!--=========== end brand section =========-->

  <!--=========== new section =========-->
  @include('includes.header')
  <!--=========== end new section =========-->

 <!--=========== Slider section =========-->
   <div class="slider">

  <div id="demo" class="carousel slide" data-ride="carousel">
  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    {{-- <li data-target="#demo" data-slide-to="1"></li>
    <li data-target="#demo" data-slide-to="2"></li> --}}
	<!-- <li data-target="#demo" data-slide-to="3"></li>
	<li data-target="#demo" data-slide-to="4"></li> -->
  </ul>

   <div class="carousel-inner">
     @php
      $sno = 1;
  @endphp
  @foreach ($pictures as $picture)
     @php
       if ($sno == 1) {
         $active = " active";
       } else {
         $active = "";
       }
     @endphp
    <div class="carousel-item {{$active}}">
      <img src="{{config('app.url')}}/slider/{{$picture->image}}" class="img-fluid slid1">
	  <div class="carousel-caption">
	    <!-- <p>Try the New Signature crafted Recipes flaver</p> -->
        <h3>MUSHROOM & SWISS</h3>
        <p>Seasond, Savory & satifying</p>

    <a href="{{$picture->link}}" target="_blank"><button type="button" class="btn btn-danger">TRY IT NOW</button></a>
      </div>
    </div>

    @php
     $sno++;
   @endphp
 @endforeach

  </div>
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
  </div>
	</div>


  <div class="clearfix"></div>
  <!--=========== end Slider section =========-->

  <!--=========== section =========-->
    <section>

      @php
        $new = HomeGrid::find(1)
      @endphp
	<div class="row">
	 <div class="col-md-6 parent" onclick="">
	 <div class="child">
     <img src="{{config('app.url')}}/grid/{{$new->image}}" alt="" class="img-responsive" style="width: 100%; height: auto">
	  <div class="text">
	    <h5>{{$new->title}}</h5>
  		<!-- <p>OUR KITCHEN, YOUR KITCHEN</p> -->
  		<a href="{{config('app.url')}}/{{$new->link}}"><button type="button" class="btn btn-default" id="sub">LEARN MORE</button></a>
	  </div>
	  </div>
	 </div>

   @php
     $new = HomeGrid::find(2)
   @endphp


	 <div class="col-md-6 parent" onclick="">
	 <div class="child">
     <img src="{{config('app.url')}}/grid/{{$new->image}}" alt="" class="img-responsive" style="width: 100%; height: auto">
	  <div class="text">
	    <h5>{{$new->title}}</h5>
		<!-- <p>YOU ASKED.WE ANSWERED</p> -->
		<a href="{{config('app.url')}}/{{$new->link}}"><button type="button" class="btn btn-default" id="sub">LEARN MORE</button></a>
	  </div>
	 </div>
	 </div>
	 </div>

   @php
     $new = HomeGrid::find(3)
   @endphp

	 <div class="row">
	 <div class="col-md-6 parent">
     <div class="child">

       <img src="{{config('app.url')}}/grid/{{$new->image}}" alt="" class="img-responsive" style="width: 100%; height: auto">
     </div>
	  <div class="text">
	    <h5>{{$new->title}}</h5>
		<!-- <p>OUR KITCHEN, YOUR KITCHEN</p> -->
		<a href="{{config('app.url')}}/{{$new->link}}"><button type="button" class="btn btn-default" id="sub">LEARN MORE</button></a>
	  </div>
	 </div>

   @php
     $new = HomeGrid::find(4)
   @endphp

	 <div class="col-md-6 parent" onclick="">
	 <div class="child">
     <img src="{{config('app.url')}}/grid/{{$new->image}}" alt="" class="img-responsive" style="width: 100%; height: auto">
	  <div class="text">
	    <h5>{{$new->title}}</h5>
		<!-- <p>YOU ASKED.WE ANSWERED</p> -->
		<a href="{{config('app.url')}}/{{$new->link}}"><button type="button" class="btn btn-default" id="sub">LEARN MORE</button></a>
	  </div>
	 </div>
	 </div>
	 </div>

   @php
     $new = HomeGrid::find(5)
   @endphp
	 <div class="row">
	 <div class="col-md-6 parent">
     <div class="child">

       <img src="{{config('app.url')}}/grid/{{$new->image}}" alt="" class="img-responsive" style="width: 100%; height: auto">
     </div>
     <div class="text">
 	    <h5>{{$new->title}}</h5>
 		<!-- <p>YOU ASKED.WE ANSWERED</p> -->
 		<a href="{{config('app.url')}}/{{$new->link}}"><button type="button" class="btn btn-default" id="sub">LEARN MORE</button></a>
 	  </div>
	 </div>

   @php
     $new = HomeGrid::find(6)
   @endphp

	 <div class="col-md-6 parent" onclick="">
	 <div class="child">
     <img src="{{config('app.url')}}/grid/{{$new->image}}" alt="" class="img-responsive" style="width: 100%; height: auto">
	  <div class="text">
	    <h5>{{$new->title}}</h5>
		<!-- <p>YOU ASKED.WE ANSWERED</p> -->
		<a href="{{config('app.url')}}/{{$new->link}}"><button type="button" class="btn btn-default" id="sub">LEARN MORE</button></a>
	  </div>
	 </div>
	 </div>
	 </div>

	</section>

   <div class="clearfix"></div>
  <!--=========== end section =========-->

  <!--=========== Footer section =========-->
   @include('includes.footer')
   <div class="clearfix"></div>
  <!--=========== end footer section =========-->

   <script>
   $(document).ready(function() {
 // executes when HTML-Document is loaded and DOM is ready
// breakpoint and up
$(window).resize(function(){
	if ($(window).width() >= 980){
      // when you hover a toggle show its dropdown menu
      $(".navbar .dropdown-toggle").hover(function () {
         $(this).parent().toggleClass("show");
         $(this).parent().find(".dropdown-menu").toggleClass("show");
       });
        // hide the menu when the mouse leaves the dropdown
      $( ".navbar .dropdown-menu" ).mouseleave(function() {
        $(this).removeClass("show");
      });

	}
});
});
   </script>
<script src="{{config('app.url')}}/assets/js/jquery.js"></script>
<script src="{{config('app.url')}}/assets/js/bootstrap.min.js"></script>
</body>

<!-- Mirrored from locksternsolutions.com/broccoli/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Feb 2019 17:15:04 GMT -->
</html>
