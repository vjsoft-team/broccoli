@php
  $page_id = 'slider_list';
  use App\Slider;
@endphp

<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from psd2allconversion.com/templates/themeforest/html/codex/v1.1/html/table_data_tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 18 Feb 2019 08:18:55 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Broccoli Slider List</title>

        <!-- Bootstrap -->
        <link href="{{config('app.url')}}/assets_admin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/waves.min.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="{{config('app.url')}}/assets_admin/css/nanoscroller.css">
        <!--        <link rel="stylesheet" href="css/nanoscroller.css">-->
        <link href="{{config('app.url')}}/assets_admin/css/menu-light.css" type="text/css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/style.css" type="text/css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/themify-icons.css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/color.css" rel="stylesheet">

    </head>
    <body class="fixed-navbar fixed-sidebar">
        <!-- Static navbar -->
        <!-- Simple splash screen-->
        <div class="splash"><div class="splash-title"><div class="spinner">
                    <img src="images/loading-new.gif" alt=""/>
                </div> </div> </div>


        @include('admin_includes.header')

        <section class="page">

            @include('admin_includes.sidebar')

            <div id="wrapper">
                <div class="content-wrapper container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title">
                                <h1>Slider Details <small></small></h1>
                                <ol class="breadcrumb">
                                    <li><a href="#"><i class="fa fa-home"></i></a></li>
                                    <li class="active">Slider List</li>
                                </ol>
                            </div>
                            @if(Session::has('message'))
                  <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                  @endif
                  @if(Session::has('messages'))
                  <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('messages') }}</p>
                  @endif
                        </div>
                    </div><!-- end .page title-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-card ">

                              @php
                                $new = Slider::all();
                              @endphp
                                <!-- Start .panel -->
                                <div class="panel-heading">
                                    <h4 class="panel-title">Slider List</h4>
                                    <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table id="basic-datatables" class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>title</th>
                                                    <th>description</th>
                                                    <th>link</th>
                                                    <th>image</th>
                                                    <th>Action</th>
                                                    {{-- <th>Start date</th>
                                                    <th>Salary</th> --}}
                                                </tr>
                                            </thead>

                                            <tbody>
                                              @foreach ($new as $key)
                                                <tr>
                                                    <td>{{$key->title}}</td>
                                                    <td>{{$key->description}}</td>
                                                    <td>{{$key->link}}</td>
                                                    <td><img src="{{config('app.url')}}/slider/{{$key->image}}"  style="width:100px;height:100px;"alt="No image"></td>
                                                    <td><div class="btn-group" role="group" aria-label="Basic example">
                                                       <a href="{{config('app.url')}}/admin/slider_edit/{{$key->id}}" button type="button" class="btn btn-primary">Edit</button></a>
                                                       <a href="{{config('app.url')}}/admin/slider/delete/{{$key->id}}" button type="button" class="btn btn-danger">Delete</button></a>
                                                       {{-- <button type="button" class="btn btn-danger">Delete</button> --}}
                                                        </div></td>

                                                    {{-- <td>Edinburgh</td>
                                                    <td>61</td>
                                                    <td>2011/04/25</td>
                                                    <td>$320,800</td> --}}
                                                </tr>
                                              @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div><!-- End .panel -->
                        </div><!--end .col-->
                    </div><!--end .row-->


                </div><div style="clear:both;"></div> </div>
        </section>
        <script type="text/javascript" src="{{config('app.url')}}/assets_admin/js/jquery.min.js"></script>
        <script type="text/javascript" src="{{config('app.url')}}/assets_admin/bootstrap/js/bootstrap.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/metisMenu.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/jquery.nanoscroller.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/pace.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/{{config('app.url')}}/assets_admin/js/jquery-jvectormap-world-mill-en.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/data-tables/jquery.dataTables.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/data-tables/dataTables.tableTools.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/data-tables/dataTables.bootstrap.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/data-tables/dataTables.responsive.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/waves.min.js"></script>
        <!--        <script src="js/jquery.nanoscroller.min.js"></script>-->
        <script type="text/javascript" src="{{config('app.url')}}/assets_admin/js/custom.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/data-tables/tables-data.js"></script>

    </body>

<!-- Mirrored from psd2allconversion.com/templates/themeforest/html/codex/v1.1/html/table_data_tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 18 Feb 2019 08:18:55 GMT -->
</html>
