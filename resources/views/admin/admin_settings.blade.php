
@php
  use App\Setting;
  $page_id = 'setting';
@endphp
<!DOCTYPE html>
<html lang="en" >

<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <title>Broccoli - Settings</title>

        <link href="{{config('app.url')}}/assets_admin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/waves.min.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="{{config('app.url')}}/assets_admin/css/nanoscroller.css">
        <link href="{{config('app.url')}}/assets_admin/css/morris-0.4.3.min.css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/menu-light.css" type="text/css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/style.css" type="text/css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/font-awesome/css/font-awesome.min.css" rel="stylesheet">


        <link href="{{config('app.url')}}/assets_admin/css/app.min.1.css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/fullcalendar.min.css" rel="stylesheet">

        <link href="{{config('app.url')}}/assets_admin/css/themify-icons.css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/color.css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/js/c3/c3.min.css" rel="stylesheet">


    </head>
    <body class="fixed-navbar fixed-sidebar">
        <!-- Static navbar -->
        <!-- Simple splash screen-->
        <div class="splash"><div class="splash-title"><div class="spinner">
                    <img src="images/loading-new.gif" alt=""/>
                </div> </div> </div>



        @include('admin_includes.header')
        <!-- Right sidebar -->

        <section class="page">

            @include('admin_includes.sidebar')

            <div id="wrapper">
                <div class="content-wrapper container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title">
                                <h1>Settings<small></small></h1>
                                <ol class="breadcrumb">
                                    <li><a href="{{config('app.url')}}/admin_home"><i class="fa fa-home"></i></a></li>
                                    <li class="active">settings</li>
                                </ol>
                            </div>
                            @if(Session::has('message'))
                  <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                  @endif</h4>
                        </div>
                    </div><!-- end .page title-->

                    <div class="row">

                    </div>
                    <div class="row">
                      @php
                        $new = Setting::all();
                        // dd($new);
                      @endphp

                        <div class="col-md-12">
                            <div class="panel panel-card margin-b-30">
                                <!-- Start .panel -->
                                <div class="panel-heading">
                                    <h4 class="panel-title">Settings</h4>
                                    {{-- <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                    </div> --}}
                                </div>
                                <div class="panel-body">
                                    <form method="post" action="{{ route('settingsUpdate') }}" class="form-horizontal">
                                      {{ csrf_field() }}
                                       @foreach ($new as $key)
                                        <div class="form-group"><label class="col-sm-2 control-label">Restaurants</label>
                                            {{-- <div class="col-sm-10"><input type="hidden"  name="id"  class="form-control"></div> --}}
                                            <div class="col-sm-10"><input type="text" value={{old('restaurants',$key->restaurants)}} name="restaurants"  class="form-control" onkeypress="return event.charCode >= 48 && event.charCode <= 57"></div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Coming Soon</label>
                                            <div class="col-sm-10"><input type="text" name="coming_soon" value={{old('coming_soon',$key->coming_soon)}}  class="form-control" onkeypress="return event.charCode >= 48 && event.charCode <= 57"></div>
                                        </div>
                                      @endforeach


                                        <div class="form-group">
                                            <div class="col-sm-4 col-sm-offset-2">
                                              <button class="btn btn-primary" type="submit">Update settings</button>
                                              <a href="{{config('app.url')}}/admin/settings"button class="btn btn-white">Cancel</button></a>
                                                {{-- <h4>Succesfully Updated</h4> --}}
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><div style="clear:both;"></div> </div>
        </section>

        <script type="text/javascript" src="{{config('app.url')}}/assets_admin/js/jquery.min.js"></script>
        <script type="text/javascript" src="{{config('app.url')}}/assets_admin/bootstrap/js/bootstrap.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/metisMenu.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/jquery.nanoscroller.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/jquery-jvectormap-1.2.2.min.js"></script>
        <!-- Flot -->
        <script src="{{config('app.url')}}/assets_admin/js/flot/jquery.flot.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/flot/jquery.flot.tooltip.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/flot/jquery.flot.resize.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/flot/jquery.flot.pie.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/flot/curved-line-chart.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/chartjs/Chart.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/pace.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/waves.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/morris_chart/raphael-2.1.0.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/morris_chart/morris.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/jquery.sparkline.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/jquery-jvectormap-world-mill-en.js"></script>

        <!--        <script src="js/jquery.nanoscroller.min.js"></script>-->
        <script type="text/javascript" src="{{config('app.url')}}/assets_admin/js/custom.js"></script>
        <!-- ChartJS-->
        <script src="{{config('app.url')}}/assets_admin/js/chartjs/Chart.min.js"></script>

        <!--page js-->
        <script src="{{config('app.url')}}/assets_admin/js/moment.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/c3/d3.v3.min.js" charset="utf-8"></script>
        <script src="{{config('app.url')}}/assets_admin/js/jquery.simpleWeather.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/fullcalendar.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/c3/c3.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/index.js"></script>

    </body>

</html>
