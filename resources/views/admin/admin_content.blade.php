@php
    $page_id = 'content';
    use App\HomeGrid;
@endphp
<!DOCTYPE html>
<html lang="en">

<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <title>Broccoli - Home Page </title>

        <link href="{{config('app.url')}}/assets_admin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/waves.min.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="{{config('app.url')}}/assets_admin/css/nanoscroller.css">
        <!--        <link rel="stylesheet" href="css/nanoscroller.css">-->
        <link href="{{config('app.url')}}/assets_admin/css/menu-light.css" type="text/css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/summernote.css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/summernote-bs3.css" rel="stylesheet">

        <link href="{{config('app.url')}}/assets_admin/css/style.css" type="text/css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/themify-icons.css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/color.css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/awesome-bootstrap-checkbox.css" rel="stylesheet">

    </head>
    <body class="fixed-navbar fixed-sidebar">
        <!-- Static navbar -->
        <!-- Simple splash screen-->
        {{-- <div class="splash"><div class="splash-title"><div class="spinner">
                    <img src="images/loading-new.gif" alt=""/>
                </div> </div> </div> --}}



        @include('admin_includes.header')
        <!-- Right sidebar -->

        <section class="page">

            @include('admin_includes.sidebar')

            @php
              $new = HomeGrid::find(1);
            @endphp

            <div id="wrapper">
                <div class="content-wrapper container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title">
                                <h1>Home Grid<small></small></h1>
                                <ol class="breadcrumb">
                                    <li><a href="{{config('app.url')}}/admin/home"><i class="fa fa-home"></i></a></li>
                                    <li class="active">Home Grid Details</li>
                                </ol>
                            </div>
                            @if(Session::has('message'))
                  <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                  @endif
                        </div>
                    </div><!-- end .page title-->

                    <div class="row">

                        <div class="col-md-12">
                            <div class="panel panel-card margin-b-30">
                                <!-- Start .panel -->
                                <div class="panel-heading">
                                    <h4 class="panel-title">Franchise</h4>
                                    <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <form method="POST" action = "{{route('Franchise')}}" class="form-horizontal" enctype="multipart/form-data">
                                      {{ csrf_field() }}
                                        <div class="form-group"><label class="col-sm-2 control-label">Title</label>
                                            <div class="col-sm-10">
                                              <input type="text" name="title" class="form-control" value="{{$new->title}}" ></div>
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="form-group"><label class="col-sm-2 control-label">description</label>
                                            <div class="col-sm-10">
                                              <input type="text" name="description" class="form-control" value="{{$new->description}}" >
                                            </div>
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="form-group"><label class="col-sm-2 control-label">Image</label>

                                            <div class="col-sm-10">
                                              <input type="file" class="form-control" name="image" class="form-control" value="{{$new->image}}" ></div>

                                              <img src="{{config('app.url')}}/grid/{{$new->image}}"  style="height:100px; width:auto; margin-left:237px;" alt="no image">
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="form-group"><label class="col-sm-2 control-label">Link</label>

                                            <div class="col-sm-10">
                                              <input type="text"name="link" class="form-control" value="{{$new->link}}"></div>
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group">
                                            <div class="col-sm-4 col-sm-offset-2">
                                                <button class="btn btn-primary" type="submit">Save changes</button>
                                                <a href="{{config('app.url')}}/admin/location" button class="btn btn-white">Cancel</a></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    @php
                      $new = HomeGrid::find(2);
                    @endphp

                    <div class="row">

                        <div class="col-md-12">
                            <div class="panel panel-card margin-b-30">
                                <!-- Start .panel -->
                                <div class="panel-heading">
                                    <h4 class="panel-title">Onine</h4>
                                    <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <form method="POST" action = "{{route('Online')}}" class="form-horizontal" enctype="multipart/form-data">
                                      {{ csrf_field() }}
                                        <div class="form-group"><label class="col-sm-2 control-label">Title</label>
                                            <div class="col-sm-10">
                                              <input type="text" name="title" class="form-control" value="{{$new->title}}" ></div>
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="form-group"><label class="col-sm-2 control-label">description</label>
                                            <div class="col-sm-10">
                                              <input type="text" name="description" class="form-control" value="{{$new->description}}" >
                                            </div>
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="form-group"><label class="col-sm-2 control-label">Image</label>

                                            <div class="col-sm-10">
                                              <input type="file" class="form-control" name="image" class="form-control" value="{{$new->image}}" ></div>

                                              <img src="{{config('app.url')}}/grid/{{$new->image}}"  style="height:100px; width:auto; margin-left:237px;" alt="no image">
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="form-group"><label class="col-sm-2 control-label">Link</label>

                                            <div class="col-sm-10">
                                              <input type="text"name="link" class="form-control" value="{{$new->link}}"></div>
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group">
                                            <div class="col-sm-4 col-sm-offset-2">
                                                <button class="btn btn-primary" type="submit">Save changes</button>
                                                <a href="{{config('app.url')}}/admin/location" button class="btn btn-white">Cancel</a></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    @php
                      $new = HomeGrid::find(3);
                    @endphp

                    <div class="row">

                        <div class="col-md-12">
                            <div class="panel panel-card margin-b-30">
                                <!-- Start .panel -->
                                <div class="panel-heading">
                                    <h4 class="panel-title">Menu</h4>
                                    <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <form method="POST" action = "{{route('Menu')}}" class="form-horizontal" enctype="multipart/form-data">
                                      {{ csrf_field() }}
                                        <div class="form-group"><label class="col-sm-2 control-label">Title</label>
                                            <div class="col-sm-10">
                                              <input type="text" name="title" class="form-control" value="{{$new->title}}" ></div>
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="form-group"><label class="col-sm-2 control-label">description</label>
                                            <div class="col-sm-10">
                                              <input type="text" name="description" class="form-control" value="{{$new->description}}" >
                                            </div>
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="form-group"><label class="col-sm-2 control-label">Image</label>

                                            <div class="col-sm-10">
                                              <input type="file" class="form-control" name="image" class="form-control" value="{{$new->image}}" ></div>

                                              <img src="{{config('app.url')}}/grid/{{$new->image}}"  style="height:100px; width:auto; margin-left:237px;" alt="no image">
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="form-group"><label class="col-sm-2 control-label">Link</label>

                                            <div class="col-sm-10">
                                              <input type="text"name="link" class="form-control" value="{{$new->link}}"></div>
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group">
                                            <div class="col-sm-4 col-sm-offset-2">
                                                <button class="btn btn-primary" type="submit">Save changes</button>
                                                <a href="{{config('app.url')}}/admin/location" button class="btn btn-white">Cancel</a></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    @php
                      $new = HomeGrid::find(4);
                    @endphp

                    <div class="row">

                        <div class="col-md-12">
                            <div class="panel panel-card margin-b-30">
                                <!-- Start .panel -->
                                <div class="panel-heading">
                                    <h4 class="panel-title">About us</h4>
                                    <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <form method="POST" action = "{{route('About')}}" class="form-horizontal" enctype="multipart/form-data">
                                      {{ csrf_field() }}
                                        <div class="form-group"><label class="col-sm-2 control-label">Title</label>
                                            <div class="col-sm-10">
                                              <input type="text" name="title" class="form-control" value="{{$new->title}}" ></div>
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="form-group"><label class="col-sm-2 control-label">description</label>
                                            <div class="col-sm-10">
                                              <input type="text" name="description" class="form-control" value="{{$new->description}}" >
                                            </div>
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="form-group"><label class="col-sm-2 control-label">Image</label>

                                            <div class="col-sm-10">
                                              <input type="file" class="form-control" name="image" class="form-control" value="{{$new->image}}" ></div>

                                              <img src="{{config('app.url')}}/grid/{{$new->image}}"  style="height:100px; width:auto; margin-left:237px;" alt="no image">
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="form-group"><label class="col-sm-2 control-label">Link</label>

                                            <div class="col-sm-10">
                                              <input type="text"name="link" class="form-control" value="{{$new->link}}"></div>
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group">
                                            <div class="col-sm-4 col-sm-offset-2">
                                                <button class="btn btn-primary" type="submit">Save changes</button>
                                                <a href="{{config('app.url')}}/admin/location" button class="btn btn-white">Cancel</a></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    @php
                      $new = HomeGrid::find(5);
                    @endphp

                    <div class="row">

                        <div class="col-md-12">
                            <div class="panel panel-card margin-b-30">
                                <!-- Start .panel -->
                                <div class="panel-heading">
                                    <h4 class="panel-title">Locations</h4>
                                    <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <form method="POST" action = "{{route('Locations')}}" class="form-horizontal" enctype="multipart/form-data">
                                      {{ csrf_field() }}
                                        <div class="form-group"><label class="col-sm-2 control-label">Title</label>
                                            <div class="col-sm-10">
                                              <input type="text" name="title" class="form-control" value="{{$new->title}}" ></div>
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="form-group"><label class="col-sm-2 control-label">description</label>
                                            <div class="col-sm-10">
                                              <input type="text" name="description" class="form-control" value="{{$new->description}}" >
                                            </div>
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="form-group"><label class="col-sm-2 control-label">Image</label>

                                            <div class="col-sm-10">
                                              <input type="file" class="form-control" name="image" class="form-control" value="{{$new->image}}" ></div>

                                              <img src="{{config('app.url')}}/grid/{{$new->image}}"  style="height:100px; width:auto; margin-left:237px;" alt="no image">
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="form-group"><label class="col-sm-2 control-label">Link</label>

                                            <div class="col-sm-10">
                                              <input type="text"name="link" class="form-control" value="{{$new->link}}"></div>
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group">
                                            <div class="col-sm-4 col-sm-offset-2">
                                                <button class="btn btn-primary" type="submit">Save changes</button>
                                                <a href="{{config('app.url')}}/admin/location" button class="btn btn-white">Cancel</a></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>



                    @php
                      $new = HomeGrid::find(6);
                    @endphp

                    <div class="row">

                        <div class="col-md-12">
                            <div class="panel panel-card margin-b-30">
                                <!-- Start .panel -->
                                <div class="panel-heading">
                                    <h4 class="panel-title">Social Media</h4>
                                    <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <form method="POST" action = "{{route('SocialMedia')}}" class="form-horizontal" enctype="multipart/form-data">
                                      {{ csrf_field() }}
                                        <div class="form-group"><label class="col-sm-2 control-label">Title</label>
                                            <div class="col-sm-10">
                                              <input type="text" name="title" class="form-control" value="{{$new->title}}" ></div>
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="form-group"><label class="col-sm-2 control-label">description</label>
                                            <div class="col-sm-10">
                                              <input type="text" name="description" class="form-control" value="{{$new->description}}" >
                                            </div>
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="form-group"><label class="col-sm-2 control-label">Image</label>

                                            <div class="col-sm-10">
                                              <input type="file" class="form-control" name="image" class="form-control" value="{{$new->image}}" ></div>

                                              <img src="{{config('app.url')}}/grid/{{$new->image}}"  style="height:100px; width:auto; margin-left:237px;" alt="no image">
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="form-group"><label class="col-sm-2 control-label">Link</label>

                                            <div class="col-sm-10">
                                              <input type="text"name="link" class="form-control" value="{{$new->link}}"></div>
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group">
                                            <div class="col-sm-4 col-sm-offset-2">
                                                <button class="btn btn-primary" type="submit">Save changes</button>
                                                <a href="{{config('app.url')}}/admin/location" button class="btn btn-white">Cancel</a></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><div style="clear:both;"></div> </div>



        </section>


        <script type="text/javascript" src="{{config('app.url')}}/assets_admin/js/jquery.min.js"></script>
        <script type="text/javascript" src="{{config('app.url')}}/assets_admin/bootstrap/js/bootstrap.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/metisMenu.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/jquery.nanoscroller.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/pace.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/waves.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/jquery-jvectormap-world-mill-en.js"></script>
        <!--        <script src="js/jquery.nanoscroller.min.js"></script>-->
        <script type="text/javascript" src="{{config('app.url')}}/assets_admin/js/custom.js"></script>
        <script type="text/javascript" src="{{config('app.url')}}/assets_admin/js/summernote/summernote.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/summernote.js"></script>
        {{-- <script src="{{config('app.url')}}/assets_admin/js/app.js"></script> --}}
    </body>

</html>
