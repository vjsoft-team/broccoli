@php
  $page_id = 'location';
@endphp
<!DOCTYPE html>

<html lang="en">

<!-- Mirrored from psd2allconversion.com/templates/themeforest/html/codex/v1.1/html/form_basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 18 Feb 2019 08:15:27 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Broccoli  Location</title>

        <!-- Bootstrap -->
        <link href="{{config('app.url')}}/assets_admin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/bootstrap-wysihtml5.css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/waves.min.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="{{config('app.url')}}/assets_admin/css/nanoscroller.css">
        <link href="{{config('app.url')}}/assets_admin/css/awesome-bootstrap-checkbox.css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/menu-light.css" type="text/css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/style.css" type="text/css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/themify-icons.css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/color.css" rel="stylesheet">
        {{-- <link href="https://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet"/> --}}
        {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.css"> --}}


    </head>
    <body class="fixed-navbar fixed-sidebar">
        <!-- Static navbar -->
        <!-- Simple splash screen-->
        <div class="splash"><div class="splash-title"><div class="spinner">
                    <img src="images/loading-new.gif" alt=""/>
                </div> </div> </div>
        <!--[if lt IE 7]>
        <p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->


          @include('admin_includes.header')

        <section class="page">

              @include('admin_includes.sidebar')


            <div id="wrapper">
                <div class="content-wrapper container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title">
                                <h1>Location<small></small></h1>
                                <ol class="breadcrumb">
                                    <li><a href="{{config('app.url')}}/admin/home"><i class="fa fa-home"></i></a></li>
                                    <li class="active">Location Details</li>
                                </ol>
                            </div>
                            @if(Session::has('message'))
                  <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                  @endif
                        </div>
                    </div><!-- end .page title-->

                    <div class="row">

                        <div class="col-md-12">
                            <div class="panel panel-card margin-b-30">
                                <!-- Start .panel -->
                                <div class="panel-heading">
                                    <h4 class="panel-title"> Address</h4>
                                    <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <form method="POST" action ="{{route('LocationStore')}} " class="form-horizontal">
                                      {{ csrf_field() }}
                                        <div class="form-group"><label class="col-sm-2 control-label">Country</label>
                                            <div class="col-sm-10">
                                              <input type="text" name="country" class="form-control"></div>
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="form-group"><label class="col-sm-2 control-label">Location</label>
                                            <div class="col-sm-10">
                                              <input type="text" name="location" class="form-control" >
                                            </div>
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="form-group"><label class="col-sm-2 control-label">Area</label>

                                            <div class="col-sm-10">
                                              <input type="text" class="form-control" name="area" class="form-control"></div>
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="form-group"><label class="col-sm-2 control-label">Mobile Number</label>

                                            <div class="col-sm-10">
                                              <input type="text"name="mobile" class="form-control"></div>
                                        </div>
                                        {{-- <div class="hr-line-dashed"></div> --}}
                                        <div class="form-group"><label class="col-lg-2 control-label">Telephone number</label>

                                            <div class="col-lg-10">
                                              <input type="text" name="telephone" class="form-control"></div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-2 control-label">Description</label>

                                            {{-- <div class="col-lg-10">
                                              <input type="text" name="description" class="form-control"></div> --}}
                                              <div class="col-lg-10">

                                                <textarea class="form-control" name="description" rows="8" cols="80"></textarea>
                                              </div>
                                        </div>
                                        <div class="form-group"><label class="col-lg-2 control-label">Carry Out</label>

                                            <div class="col-lg-10">
                                              <input  type="text" name="carry_out" class="form-control">
                                            </div>

                                        </div>
                                        <div class="form-group"><label class="col-lg-2 control-label">Delivery</label>

                                            <div class="col-lg-10">
                                              <input type="text" name="delivery" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group"><label class="col-lg-2 control-label">Latitude</label>

                                            <div class="col-lg-10">
                                              <input type="text" name="latitude" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group"><label class="col-lg-2 control-label">Longitude</label>

                                            <div class="col-lg-10">
                                              <input type="text" name="longitude" class="form-control">
                                            </div>
                                        </div>


                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group">
                                            <div class="col-sm-4 col-sm-offset-2">
                                                <button class="btn btn-primary" type="submit">Save changes</button>
                                                <a href="{{config('app.url')}}/admin/location" button class="btn btn-white">Cancel</a></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><div style="clear:both;"></div> </div>
        </section>
        <script type="text/javascript" src="{{config('app.url')}}/assets_admin/js/jquery.min.js"></script>
        <script type="text/javascript" src="{{config('app.url')}}/assets_admin/bootstrap/js/bootstrap.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/metisMenu.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/jquery.nanoscroller.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/waves.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/pace.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/jquery-jvectormap-world-mill-en.js"></script>
        <!--        <script src="js/jquery.nanoscroller.min.js"></script>-->
        <script type="text/javascript" src="{{config('app.url')}}/assets_admin/js/custom.js"></script>
        {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.min.js"></script>
        <script>
              $('.clockpicker').clockpicker({
          placement: 'top',
          align: 'left',
          donetext: 'Done'
      });
        </script> --}}

    </body>

<!-- Mirrored from psd2allconversion.com/templates/themeforest/html/codex/v1.1/html/form_basic.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 18 Feb 2019 08:15:27 GMT -->
</html>
