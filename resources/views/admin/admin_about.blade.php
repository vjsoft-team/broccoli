@php
    $page_id = 'about';
    use App\About;
@endphp
<!DOCTYPE html>
<html lang="en">

<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <title>Broccoli - About us</title>

        <link href="{{config('app.url')}}/assets_admin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/waves.min.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="{{config('app.url')}}/assets_admin/css/nanoscroller.css">
        <!--        <link rel="stylesheet" href="css/nanoscroller.css">-->
        <link href="{{config('app.url')}}/assets_admin/css/menu-light.css" type="text/css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/summernote.css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/summernote-bs3.css" rel="stylesheet">

        <link href="{{config('app.url')}}/assets_admin/css/style.css" type="text/css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/themify-icons.css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/color.css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/awesome-bootstrap-checkbox.css" rel="stylesheet">

    </head>
    <body class="fixed-navbar fixed-sidebar">
        <!-- Static navbar -->
        <!-- Simple splash screen-->
        <div class="splash"><div class="splash-title"><div class="spinner">
                    <img src="images/loading-new.gif" alt=""/>
                </div> </div> </div>



        @include('admin_includes.header')
        <!-- Right sidebar -->

        <section class="page">

            @include('admin_includes.sidebar')

            <div id="wrapper">
                <div class="content-wrapper container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-title">
                                <h1>Story <small></small></h1>
                                <ol class="breadcrumb">
                                    <li><a href="{{config('app.url')}}/admin/home"><i class="fa fa-home"></i></a></li>
                                    <li class="active">About us</li>
                                </ol>
                            </div>
                            @if(Session::has('message'))
                  <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                  @endif
                        </div>
                    </div><!-- end .page title-->

<!-- Who we are-->

                    <div class="row">
                        <div class="col-sm-12">
                          @php
                            $new = About::find(1);
                          @endphp

                          <form class="" action="{{ route('aboutUpdate') }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}

                            <div class="panel panel-card recent-activites">
                                <!-- Start .panel -->
                                <div class="panel-heading">
                                    <h4 class="panel-title">The Story</h4>
                                  </div>




                              <div class="form-group">
                                <textarea class="summernote" name="story" value ="{{$new->story}}" rows="8" cols="80"> {{$new->story}}</textarea>
                              </div>


                            </div><!-- End .panel -->
                              <div class="form-group">
                                  <div class=""><input type="file" name="image"  value="{{$new->image}}" class="form-control"></div>
                                  <br>
                                  <div class="">
                                    <img src="{{config('app.url')}}/about/{{$new->image}}"  style="width:100px;height:100px;"alt="No image">
                                  </div>

                                  @if ($errors->has('image'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('image') }}</strong>
                                      </span>
                                  @endif
                              </div>



                              <div class="form-group">
                                  <div class="">
                                      {{-- <button class="btn btn-white" type="submit">Cancel</button> --}}
                                      <button class="btn btn-primary" type="submit">Save changes</button>
                                  </div>
                              </div>

                            </form>
                        </div>

                    </div>
                    <br><br>

<!-- Who we do-->
                    <div class="row">
                        <div class="col-sm-12">
                          @php
                          $new = About::find(2);
                          @endphp

                          <form class="" action="{{ route('We') }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="panel panel-card recent-activites">
                                <!-- Start .panel -->
                                <div class="panel-heading">
                                    <h4 class="panel-title">Who We are</h4>

                                    {{-- <div class="panel-actions">
                                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                    </div> --}}

                                  </div>
                                      {{-- <div class="form-group">
                                        <div class="">
                                          <input class="form-control" type="text" name="title" value="{{$new->title}}">
                                        </div>
                                      </div> --}}
                              <div class="form-group">
                                <textarea class="summernote" name="story" value = "{{$new->story}}" rows="8" cols="80"> {{$new->story}}</textarea>
                              </div>

                            </div><!-- End .panel -->
                              <div class="form-group">
                                  <div class=""><input type="file" name="image" value="{{$new->image}}" class="form-control"></div>
                                  <br>
                                  <div class="">
                                    <img src="{{config('app.url')}}/about/{{$new->image}}"  style="width:100px;height:100px;"alt="no image">
                                  </div>
                                  @if ($errors->has('image'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('image') }}</strong>
                                      </span>
                                  @endif
                              </div>



                              <div class="form-group">
                                  <div class="">
                                      {{-- <button class="btn btn-white" type="submit">Cancel</button> --}}
                                      <button class="btn btn-primary" type="submit">Save changes</button>
                                  </div>
                              </div>

                            </form>
                        </div>

                    </div>
                    <br><br><br>
<!-- Mission-->
                    <div class="row">
                        <div class="col-sm-12">
                          @php
                          $new = About::find(3);
                          @endphp
                          <form class="" action="{{route('Mission')}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="panel panel-card recent-activites">
                                <!-- Start .panel -->
                                <div class="panel-heading">
                                    <h4 class="panel-title">What We do</h4>
                                  </div>

                              {{-- <div class="form-group">
                                <div class="">
                                  <input class="form-control" type="text" name="title" value="{{$new->title}}">
                                </div>
                              </div> --}}

                              <div class="form-group">
                                <textarea class="summernote" name="story" value = "{{$new->story}}" rows="8" cols="80"> {{$new->story}}</textarea>
                              </div>

                            </div><!-- End .panel -->
                              <div class="form-group">
                                  <div class=""><input type="file" name="image" class="form-control" value="{{$new->image}}"></div>
                                  <br>
                                  <div class="">
                                    <img src="{{config('app.url')}}/about/{{$new->image}}" style="width:100px;height:100px;"alt="no image">
                                    @if ($errors->has('image'))
                                      <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('image') }}</strong>
                                      </span>
                                    @endif
                                  </div>
                              </div>



                              <div class="form-group">
                                  <div class="">
                                      {{-- <button class="btn btn-white" type="submit">Cancel</button> --}}
                                      <button class="btn btn-primary" type="submit">Save changes</button>
                                  </div>
                              </div>

                            </form>
                        </div>

                    </div>
                    <br><br><br>
  <!-- story-->
                    <div class="row">
                        <div class="col-sm-12">
                          @php
                          $new = About::find(4);
                          @endphp

                          <form class="" action="{{route('Story')}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="panel panel-card recent-activites">
                                <!-- Start .panel -->
                                <div class="panel-heading">
                                    <h4 class="panel-title">Our Mission</h4>
                                  </div>
                                {{-- <div class="form-group">
                                  <div class="">
                                    <input class="form-control" type="text" name="title" value="{{$new->title}}">
                                  </div>
                                </div> --}}

                              <div class="form-group">
                                <textarea class="summernote" name="story" value = "{{$new->story}}" rows="8" cols="80"> {{$new->story}}</textarea>
                              </div>

                            </div><!-- End .panel -->
                              <div class="form-group">
                                  <div class=""><input type="file" name="image" class="form-control" value="{{$new->image}}"></div>
                                  <br>
                                  <div class="">
                                    <img src="{{config('app.url')}}/about/{{$new->image}}"  style="width:100px;height:100px;"alt="no image">
                                    @if ($errors->has('image'))
                                      <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('image') }}</strong>
                                      </span>
                                    @endif
                                  </div>
                              </div>


                              <div class="form-group">
                                  <div class="">
                                      {{-- <button class="btn btn-white" type="submit">Cancel</button> --}}
                                      <button class="btn btn-primary" type="submit">Save changes</button>
                                  </div>
                              </div>

                            </form>
                        </div>

                    </div>
                    <br><br><br>
  <!-- Experience-->
                    <div class="row">
                        <div class="col-sm-12">
                          @php
                          $new = About::find(5);
                          @endphp

                          <form class="" action="{{route('Experience')}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="panel panel-card recent-activites">
                                <!-- Start .panel -->
                                <div class="panel-heading">
                                    <h4 class="panel-title">The Experience</h4>
                                  </div>

                                      {{-- <div class="form-group">
                                        <div class="">
                                          <input class="form-control" type="text" name="title" value="{{$new->title}}">
                                        </div>
                                      </div> --}}


                              <div class="form-group">
                                <textarea class="summernote" name="story" value = "{{$new->story}}" rows="8" cols="80"> {{$new->story}}</textarea>
                              </div>

                            </div><!-- End .panel -->
                              <div class="form-group">
                                  <div class=""><input type="file" name="image" value="{{$new->image}}" class="form-control"></div>
                                  <br>
                                  <div class="">
                                    <img src="{{config('app.url')}}/about/{{$new->image}}"  style="width:100px;height:100px;"alt="No image">
                                    @if ($errors->has('image'))
                                      <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('image') }}</strong>
                                      </span>
                                    @endif
                                  </div>
                              </div>

                              <div class="form-group">
                                  <div class="">
                                      {{-- <button class="btn btn-white" type="submit">Cancel</button> --}}
                                      <button class="btn btn-primary" type="submit">Save changes</button>
                                  </div>
                              </div>

                            </form>
                        </div>

                    </div>

                </div><div style="clear:both;"></div> </div>


        </section>


        <script type="text/javascript" src="{{config('app.url')}}/assets_admin/js/jquery.min.js"></script>
        <script type="text/javascript" src="{{config('app.url')}}/assets_admin/bootstrap/js/bootstrap.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/metisMenu.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/jquery.nanoscroller.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/pace.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/waves.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/jquery-jvectormap-world-mill-en.js"></script>
        <!--        <script src="js/jquery.nanoscroller.min.js"></script>-->
        <script type="text/javascript" src="{{config('app.url')}}/assets_admin/js/custom.js"></script>
        <script type="text/javascript" src="{{config('app.url')}}/assets_admin/js/summernote/summernote.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/summernote.js"></script>
        {{-- <script src="{{config('app.url')}}/assets_admin/js/app.js"></script> --}}
    </body>

</html>
