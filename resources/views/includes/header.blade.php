@php
  use App\Setting;

  $new = Setting::find(1);
@endphp


<div class="top-bar">
  <div class="container">
 <div class="row">
     <div class="col-md-5 col-sm-5 offset-md-3">
   <div class="right-top">
   <span class="top-menu">Restaurants:<span class="top-menu1"> Under Operation: {{$new->restaurants}} | Coming Soon:{{$new->coming_soon}} | Total:{{$new->restaurants + $new->coming_soon}}</p>
   </div>
   </div>

   <div class="col-md-4 col-sm-4">
   <div class="icon">
     <ul>
<li><a href="https://www.facebook.com/Broccolipizzaandpasta" target="_blank"><img src="{{config('app.url')}}/assets/icons/fb.svg" alt="" style="height: 25px;"></a></li>
<li><a href="https://www.instagram.com/broccolipizzaandpasta/" target="_blank"><img src="{{config('app.url')}}/assets/icons/in.svg" alt="" style="height: 25px;"></a></li>
<li><a href="https://twitter.com/BroccoliPasta" target="_blank"><img src="{{config('app.url')}}/assets/icons/tw.svg" alt="" style="height: 25px;"></a></li>
<li><a href="https://www.youtube.com/channel/UC0gOEO15Oy6j6mIqE_dBFmA?view_as=subscriber" target="_blank"><img src="{{config('app.url')}}/assets/icons/yt.svg" alt=""
    style="height: 25px;"></a></li>
<li><a href="https://vimeo.com/user17747599" target="_blank"><img src="{{config('app.url')}}/assets/icons/vm.svg" alt="" style="height: 25px;"></a></li>
<li><a href="https://plus.google.com/u/0/111459393741121825340" target="_blank"><img src="{{config('app.url')}}/assets/icons/go.svg" alt="" style="height: 25px;"></a></li>


     </ul>
   </div>
   </div>

 </div>
</div>
</div>

<div class="clearfix"></div>

<div class="bg-light">
 <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
 <a class="navbar-brand" href="{{config('app.url')}}/"><img src="{{config('app.url')}}/assets/img/logo.png" class="img-fluid"></a>
 <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
  <span class="navbar-toggler-icon"></span>
 </button>

 <div class="collapse navbar-collapse" id="navbarSupportedContent">
  <ul class="navbar-nav mr-auto">
    <li class="nav-item Active">
      <a class="nav-link" href="{{config('app.url')}}/">HOME</a>
    </li>

  <li class="nav-item">
      <a class="nav-link" href="{{config('app.url')}}/about-us">ABOUT</a>
    </li>
  <!-- <li class="nav-item">
      <a class="nav-link" href="trade-show.html">TRADE SHOW</a>
    </li> -->
  <li class="nav-item">
      <a class="nav-link" href="{{config('app.url')}}/career">CAREER</a>
    </li>
  <li class="nav-item">
<a class="nav-link" href="{{config('app.url')}}/contact-us">CONTACT US</a>
    </li>


           <li class="nav-item dropdown">
           <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          FRANCHISE
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <div class="container">
          <div class="row">

              <div class="col-md-6">
              <ul class="nav flex-column">
              <li class="nav-item">
              <a class="nav-link active" href="{{config('app.url')}}/franchise-application">Franchise Application</a>
              </li>

              <li class="nav-item">
              <a class="nav-link" href="#" style="line-height: 1.3">Business Development Agent Application</a>
              </li>

              <li class="nav-item">
              <a class="nav-link" href="{{config('app.url')}}/steps-for-franchising">Steps for Franchising</a>
              </li>
              </ul>
              </div>

              <div class="col-md-3">
              <ul class="nav flex-column">
              <li class="nav-item">
              <a class="nav-link" href="{{config('app.url')}}/attend">Attent a Seminar</a>
              </li>
              <li class="nav-item">
              <a class="nav-link" href="{{config('app.url')}}/trade-show">Trade Show</a>
              </li>
              <li class="nav-item">
              <a class="nav-link" href="{{config('app.url')}}/franchise-faq">FAQ</a>
              </li>

              <!-- <li class="nav-item">
              <a class="nav-link" href="#">Link item</a>
              </li>

              <li class="nav-item">
              <a class="nav-link" href="#">Link item</a>
              </li> -->
              </ul>
              </div>

      <div class="col-md-3">
              <ul class="nav flex-column">
              <li class="nav-item">
              <img src="{{config('app.url')}}/assets/img/franchise.jpg" alt="" style="width: 250px">
              </li>
              </ul>
             </div>

          </div>
        </div>
        </div>
       </li>

       <li class="nav-item">
   <a class="nav-link" href="{{config('app.url')}}/FAQ">FAQ</a>
         </li>

  <li class="nav-item">
    <a class="nav-link" href="{{config('app.url')}}/location"><i class="fa fa-map-marker"></i> LOCATION</a>
    </li>

  <li class="nav-item float-right">
<a class="nav-link" href="#"> العربية <img src="{{config('app.url')}}/assets/icons/arabia-flag.png" alt="" style="width: 25px"></a>
    </li>

      </ul>
 </div>
</nav>
</div>

<div class="clearfix"></div>

<div class="sub-menu">
<div class="container">
<div class="row">
<div class="col-md-12">
  <div class="new">
  <span class="sub2">START YOUR ORDER </span>
  <button type="button" class="btn btn-default" id="sub1">DELIVERY</button>
 <span class="sub3"> OR</span>
 <button type="button" class="btn btn-default" id="sub1">CARRYOUT</button>

  </span>
</div>
</div>
</div>
</div>
</div>
<div class="clearfix"></div>
