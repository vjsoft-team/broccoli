<footer class="footer">
  <div class="container">
 <div class="row">

 <!--=============== end mobile section =================-->
   <div class="col-md-12 new-foot" id="new-foot4">
  <p>Stay Connected</p>
  <a href="https://www.facebook.com/Broccolipizzaandpasta" target="_blank" class="fa fa-facebook"></a>
  <a href="https://twitter.com/BroccoliPasta" target="_blank" class="fa fa-twitter"></a>
  <a href="https://www.youtube.com/channel/UC0gOEO15Oy6j6mIqE_dBFmA?view_as=subscriber" target="_blank" class="fa fa-youtube"></a>
  <a href="https://www.instagram.com/broccolipizzaandpasta/" class="fa fa-instagram" target="_blank" ></a>
  <a href="https://vimeo.com/user17747599" class="fa fa-tumblr" target="_blank" ></a>
  <!-- <a href="https://plus.google.com/111459393741121825340" target="_blank" class="fa fa-rss"></a> -->
  <hr>
  <p>Get the Broccoli App</p>
  <div class="col-md-12 col-sm-12">

    <a href="#" id="apple"><img src="{{config('app.url')}}/assets/img/logo-apple.png" class="img-fluid"></a>
  </div>
  <div class="col-md-12 col-sm-12">
    <br>
  </div>
  <div class="col-md-12 col-sm-12">

    <a href="#" id="apple"><img src="{{config('app.url')}}/assets/img/logo-google.png" class="img-fluid"></a>
  </div>
  <p></p>
  <a href="#" class="apple">Contact </a>
  <a href="#" class="apple">Provacy(updated)</a>
 <p></p>
  <a href="#" class="apple">Terms & Conditions </a>
  <a href="#" class="apple">Accessibility</a>
  <p class="new1">Copyright © 2019 &nbsp; Broccoli. All Rights Reserved</p>
 </div>

<!--=============== end mobile section =================-->

   <div class="col-md-12">
  <a href="{{config('app.url')}}/"><img src="{{config('app.url')}}/assets/img/logo.png" class="img-fluid" id="two"></a>
 </div>

 <div class="col-md-5 foot1">
  <p>BROCCOLI Pizza & Pasta Offers Freshly Prepared Italian Food and Personalized Meals For a Health-Conscious Community. The Name Barccoli Speaks For Itself-This Nutrious Vegetable is a Symbol Of Our Dedication To Healthy Eating And Quality Product.</p>
 </div>

 <div class="col-md-2 foot1">
   <h6>Products</h6>
   <ul>
     <li><a href="#">Price Drop</a></li>
   <li><a href="#">New Products</a></li>
   <li><a href="#">Best Sales</a></li>
   <li><a href="#">Contact us</a></li>
   <li><a href="#">Sitemap</a></li>
   </ul>
 </div>

 <div class="col-md-2 foot1">
   <h6>Our Company</h6>
   <ul>
     <li><a href="#">Delivery</a></li>
   <li><a href="#">Legal Notice</a></li>
   <li><a href="#">Our Team</a></li>
   <li><a href="#">About Flavours</a></li>
   <li><a href="#">Blog</a></li>
   </ul>
 </div>

 <div class="col-md-2 foot1">
   <h6>Information</h6>
   <ul>
     <li><a href="#">Your Orders</a></li>
   <li><a href="#">Your Wishlist</a></li>
   <li><a href="#">Payment Return</a></li>
   <li><a href="#">Wheel Shine</a></li>
   <li><a href="#">Your Account</a></li>
   </ul>
 </div>
 <!-- <p>Get the Broccoli App</p> -->
 <!-- <div class="col-md-6 col-sm-6">
 </div> -->
 <div class="col-md-1 col-sm-1 foot1 get-app">
   <h6>App's</h6>
   <a href="#" id="apple"><img src="{{config('app.url')}}/assets/img/logo-apple.png" class="" style=""></a>
   <br>
   <br>
   <a href="#" id="apple"><img src="{{config('app.url')}}/assets/img/logo-google.png" class=""></a>
 </div>

 <div class="col-md-12 fo2">
  <div class="fo">
    <ul>
     <li style="border-left: none;"><a href="{{config('app.url')}}/">HOME</a></li>
   <li><a href="https://www.broccolidelivery.com/">ONLINE</a></li>
   <li><a href="{{config('app.url')}}/trade-show">TRADE SHOWS</a></li>
   <li><a href="{{config('app.url')}}/career">CAREERS</a></li>
   <li><a href="{{config('app.url')}}/steps-for-franchising">FRANCHISE</a></li>
   <li><a href="{{config('app.url')}}/FAQ">FAQ'S</a></li>
   <li><a href="https://www.broccolipizzaandpasta.com/media/">MEDIA</a></li>
   <li><a href="https://www.broccolidelivery.co.uk/">UK WEBSITE</a></li>
   <li><a href="{{config('app.url')}}/contact-us">CONTACT US</a></li>
   </ul>
  </div>
 </div>

 </div>
</div>
</footer>
