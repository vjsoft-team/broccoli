{{-- @php
  use App\About;
@endphp --}}

<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from locksternsolutions.com/broccoli/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Feb 2019 17:11:18 GMT -->
<head>
<title>Broccoli - Franchise Application</title>
  <meta charset="UTF-8">
  <meta name="keywords" content="HTML,CSS,XML,JavaScript">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Site Icons -->
  <link href="{{config('app.url')}}/assets/img/icon.jpg" type="{{config('app.url')}}/assets/img/Home-512.png" rel="icon">

  <!-- font-icon -->
  <link rel="stylesheet" href="{{config('app.url')}}/assets/font-awesome/css/font-awesome.min.css">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/assets/css/bootstrap.min.css">

  <!-- Custom CSS -->
  <link rel="stylesheet" href="{{config('app.url')}}/assets/style.css">
  <link href="https://fonts.googleapis.com/css?family=Josefin+Sans|PT+Sans" rel="stylesheet">
  <!-- <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet"> -->

</head>
<body>
<!--=========== top head =========-->


<!--=========== end top head =========-->

<!--=========== Navbar section =========-->


 <!--=========== end brand section =========-->

  <!--=========== new section =========-->
  @include('includes.header')
  <!--=========== end new section =========-->

 <!--=========== Slider section =========-->



  <div class="clearfix"></div>
  <!--=========== end Slider section =========-->

  <!--=========== section =========-->

  <section class="franchise">
<p><br><p>
<div class="container">
   <div class="row" id="piz">
     <div class="col-md-3 piz">
     <img src="{{config('app.url')}}/assets/img/fre.jpg" class="img-fluid">
   </div>

   <div class="col-md-6">
     <div class="PIZZA">
       <h6>BROCCOLI PIZZA AND PASTA</h6>
     <p>Welcome to Broccoli Pizza & Pasta, one of the fastest growing franchises in the world. We are committed to the future of our business and we invite others to become part of our profitable system by applying for franchise opportunities below.</p>
     </div>
   </div>

   <div class="col-md-3 piz">
     <img src="{{config('app.url')}}/assets/img/fre.jpg" class="img-fluid">
   </div>
   </div>

      <div class="row">
    <div class="col-md-4"></div>
      <div class="application">
        <p>Franchise Application</p>
      </div>
    <div class="col-md-4"></div>
    </div>

     <div class="row">
       <div class="col-md-1"></div>
       <div class="form-group col-md-5 col-sm-5">
           <label for="name" class="label">First Name :</label>
           <input type="text" class="form-control input-sm" id="name" placeholder="">
           </div>

     <div class="form-group col-md-5 col-sm-5">
           <label for="name" class="label">Last Name: :</label>
           <input type="text" class="form-control input-sm" id="name" placeholder="">
           </div>
     <div class="col-md-1"></div>
       </div>

      <div class="row">
       <div class="col-md-1"></div>
       <div class="form-group col-md-5 col-sm-5">
           <label for="name" class="label">Email :</label>
           <input type="text" class="form-control input-sm" id="name" placeholder="">
           </div>

     <div class="form-group col-md-5 col-sm-5">
           <label for="name" class="label">Phone (Mobile): :</label>
           <input type="text" class="form-control input-sm" id="name" placeholder="">
           </div>
     <div class="col-md-1"></div>
       </div>

     <div class="row">
       <div class="col-md-1"></div>
       <div class="form-group col-md-5 col-sm-5">
           <label for="name" class="label">City :</label>
           <input type="text" class="form-control input-sm" id="name" placeholder="">
           </div>

     <div class="form-group col-md-5 col-sm-5">
           <label for="name" class="label">How much you are looking to invest in this venture? :</label>
           <select id="insightly_Title" class="form-control" name="Title">
     <option value="Choose one:">Select option</option>
     <option value="">1</option>
     <option value="">2</option>
     <option value="">3</option>
     <option value="">4</option>
     </select>
           </div>
     <div class="col-md-1"></div>
       </div>

     <div class="row">
       <div class="col-md-1"></div>
       <div class="form-group col-md-5 col-sm-5">
           <label for="name" class="label">Country :</label>
           <select id="insightly_Title" class="form-control" name="Title">
     <option value="Choose one:">Select Country</option>
     <option value="">1</option>
     <option value="">2</option>
     <option value="">3</option>
     <option value="">4</option>
     </select>
           </div>

     <div class="form-group col-md-5 col-sm-5">
           <label for="name" class="label">Description :</label>
           <input type="text" class="form-control input-sm" id="name" placeholder="">
           </div>
     <div class="col-md-1"></div>
       </div>

     <div class="row">
       <div class="col-md-1"></div>
       <div class="form-group col-md-5 col-sm-5">
            <button type="submit" class="btn btn-success">Submit</button>
           </div>

       </div>

</div>
<br>
 </section>

   <div class="clearfix"></div>
  <!--=========== end section =========-->

  <!--=========== Footer section =========-->
   @include('includes.footer')
   <div class="clearfix"></div>
  <!--=========== end footer section =========-->

   <script>
   $(document).ready(function() {
 // executes when HTML-Document is loaded and DOM is ready
// breakpoint and up
$(window).resize(function(){
	if ($(window).width() >= 980){
      // when you hover a toggle show its dropdown menu
      $(".navbar .dropdown-toggle").hover(function () {
         $(this).parent().toggleClass("show");
         $(this).parent().find(".dropdown-menu").toggleClass("show");
       });
        // hide the menu when the mouse leaves the dropdown
      $( ".navbar .dropdown-menu" ).mouseleave(function() {
        $(this).removeClass("show");
      });

	}
});
});
   </script>
<script src="{{config('app.url')}}/assets/js/jquery.js"></script>
<script src="{{config('app.url')}}/assets/js/bootstrap.min.js"></script>
</body>

<!-- Mirrored from locksternsolutions.com/broccoli/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Feb 2019 17:15:04 GMT -->
</html>
