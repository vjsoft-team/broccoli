
<!DOCTYPE html>
<html lang="en">


<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Broccoli - Login</title>

        <!-- Bootstrap -->
        <link href="{{config('app.url')}}/assets_admin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/waves.min.css" type="text/css" rel="stylesheet"><link rel="stylesheet" href="css/nanoscroller.css">
        <!--        <link rel="stylesheet" href="css/nanoscroller.css">-->
        <link href="{{config('app.url')}}/assets_admin/css/style.css" type="text/css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/themify-icons.css" rel="stylesheet">
        <link href="{{config('app.url')}}/assets_admin/css/color.css" rel="stylesheet">

    </head>
    <body class="account">
        <div class="container">
            <div class="row">
                <div class="account-col text-center">
                    <h1>Broccoli-Admin</h1>
                    <h3>Log into your account</h3>
                    <form class="m-t" role="form" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="E-mail" required="">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Passowrd" required="">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary btn-block ">Login</button>
                        {{-- <a href="{{ route('password.request') }}"><small>Forgot password?</small></a>
                        <p class=" text-center"><small>Do not have an account?</small></p>
                        <a class="btn  btn-default btn-block" href="register.html">Create an account</a> --}}
                        <p>Broccoli &copy; 2019</p>
                    </form>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="{{config('app.url')}}/assets_admin/js/jquery.min.js"></script>
        <script type="text/javascript" src="{{config('app.url')}}/assets_admin/bootstrap/js/bootstrap.min.js"></script>
        <script src="{{config('app.url')}}/assets_admin/js/pace.min.js"></script>
    </body>

<!-- Mirrored from psd2allconversion.com/templates/themeforest/html/codex/v1.1/rtl/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 16 Feb 2019 08:50:07 GMT -->
</html>
