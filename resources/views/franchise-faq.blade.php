{{-- @php
  use App\About;
@endphp --}}

<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from locksternsolutions.com/broccoli/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Feb 2019 17:11:18 GMT -->
<head>
<title>Broccoli - FAQ</title>
  <meta charset="UTF-8">
  <meta name="keywords" content="HTML,CSS,XML,JavaScript">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Site Icons -->
  <link href="{{config('app.url')}}/assets/img/icon.jpg" type="{{config('app.url')}}/assets/img/Home-512.png" rel="icon">

  <!-- font-icon -->
  <link rel="stylesheet" href="{{config('app.url')}}/assets/font-awesome/css/font-awesome.min.css">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/assets/css/bootstrap.min.css">

  <!-- Custom CSS -->
  <link rel="stylesheet" href="{{config('app.url')}}/assets/style.css">
  <link href="https://fonts.googleapis.com/css?family=Josefin+Sans|PT+Sans" rel="stylesheet">
  <!-- <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet"> -->



</head>
<body>
<!--=========== top head =========-->


<!--=========== end top head =========-->

<!--=========== Navbar section =========-->


 <!--=========== end brand section =========-->

  <!--=========== new section =========-->
  @include('includes.header')
  <!--=========== end new section =========-->

 <!--=========== Slider section =========-->



  <div class="clearfix"></div>
  <!--=========== end Slider section =========-->

  <!--=========== section =========-->
  {{-- @php
    $new = About::find(1);
  @endphp --}}
  <section class="faq">
<p><br><p>
<div class="container">
     <div class="row">
   <h6>FAQ</h6>
     <div id="accordion">

        <div class="card">
        <div class="card-header" data-toggle="collapse" href="#collapseOne">
        <h4 class="card-link">
        <i class="fa fa-angle-double-right"></i> What is the concept of Broccoli Pizza & Pasta?
        </h4>
       </div>
       <div id="collapseOne" class="collapse show" data-parent="#accordion">
       <div class="card-body">
         <ul>
           <li>Broccoli Pizza & Pasta is a specialized pizza and pasta concept where food is prepared fresh on the spot. The pizza dough, pasta along with their sauces is freshly made in the store. The menu presents a wide selection of pizza, pasta and salads inspired from all around the globe.</li>
           <li>Broccoli Pizza & Pasta provides best franchise in Dubai such as pizza franchise or pasta franchise.</li>
           <li>At Broccoli Pizza & Pasta, our goal is to promote healthy fresh pizzas and pastas in the best quality with an impeccable service</li>
           <li>The first unit of Broccoli Pizza & Pasta was opened in 2011 in Tecom, in the center of Dubai. Currently there are three Broccoli Pizza & Pasta restaurants operating in prime locations in Dubai at Jumeirah Beach Road and Tecom.</li>
           <li>Broccoli Pizza & Pasta serves multicultural middle-low to high-end income customers. The size of a typical Broccoli Pizza & Pasta restaurant is between 400 sq.feet and 1,800 sq.feet with a customer flow of 200 to 270 checks per day.</li>
           <li><a href="docs/UAE-FAQ.pdf">UAE PRICES</a> &nbsp;&nbsp;&nbsp; <a href="docs/UK-FAQ.pdf">UK PRICES</a></li>
         </ul>
       </div>
       </div>
       </div>

       <div class="card">
       <div class="card-header" data-toggle="collapse" href="#collapseTwo">
       <h4 class="card-link">
       <i class="fa fa-angle-double-right"></i> What is the investment required?
       </h4>
       </div>
       <div id="collapseTwo" class="collapse" data-parent="#accordion">
       <div class="card-body">
         The initial investment for an individual unit ranges from  $160,000 to $ 360,000 ($190,000 on average) this includes Franchise Fee and Training. This includes 2 months operational cost and 6 months rent.
       </div>
       </div>
       </div>


   <div class="card">
       <div class="card-header" data-toggle="collapse" href="#collapse3">
       <h4 class="card-link">
        <i class="fa fa-angle-double-right"></i> How much is the initial Franchise Fee?
       </h4>
       </div>
       <div id="collapse3" class="collapse" data-parent="#accordion">
       <div class="card-body">
         The initial franchise fee is $12,500, which includes 2 weeks full management training, operations manuals, site selection and evaluation, initial in store pre marketing, contractor and supplier procurement, negotiations and any help you need to get the restaurant open.
       </div>
       </div>
       </div>

   <div class="card">
       <div class="card-header" data-toggle="collapse" href="#collapse4">
       <h4 class="card-link">
        <i class="fa fa-angle-double-right"></i>  How much is the fee for continuing support services (the ‘Royalty Fee’ and Advertising fee) ?
        </h4>
       </div>
       <div id="collapse4" class="collapse" data-parent="#accordion">
       <div class="card-body">
         The continuing services, or ‘royalty fee’ is 6% of your gross sales paid weekly. This fee entitles you to use the Broccoli Pizza & Pasta service mark, use of distinctive system, marketing assistance, ongoing business development and counseling, and other benefits that come with being a Broccoli Pizza & Pasta franchisee.
       </div>
       </div>
       </div>


   <div class="card">
       <div class="card-header" data-toggle="collapse" href="#collapse5">
       <h4 class="card-link">
        <i class="fa fa-angle-double-right"></i> Will I have an exclusive territory?
        </h4>
       </div>
       <div id="collapse5" class="collapse" data-parent="#accordion">
       <div class="card-body">
         Broccoli Pizza & Pasta does not grant an exclusive territory.
       </div>
       </div>
       </div>
   <div class="card">
       <div class="card-header" data-toggle="collapse" href="#collapse6">
       <h4 class="card-link">
        <i class="fa fa-angle-double-right"></i> What about advertising?
        </h4>
       </div>
       <div id="collapse6" class="collapse" data-parent="#accordion">
       <div class="card-body">
         The Broccoli Pizza & Pasta name and reputation is an important part of our business. Each franchisee pays 3% of their weekly revenue into a communal marketing fund that is spent at the discretion of the franchisee marketing board to benefit the franchisees. On top of this you are expected to spend money on in store offer and menus to attract your customers will spend at least 3% of your gross sales per week on marketing. Voting for the marketing fund board in each country takes place once every 2 years or when someone resigns.
       </div>
       </div>
       </div>
   <div class="card">
       <div class="card-header" data-toggle="collapse" href="#collapse7">
       <h4 class="card-link">
        <i class="fa fa-angle-double-right"></i>  How much training is provided in the Broccoli Pizza & Pasta system?
        </h4>
       </div>
       <div id="collapse7" class="collapse" data-parent="#accordion">
       <div class="card-body">
         <ul>
           <li>Broccoli Pizza & Pasta Management will provide franchisees with approximately 10 to 14 days of initial training at Broccoli Pizza & Pasta headquarters or at a location designated by the Broccoli Pizza & Pasta Management, beginning approximately 6 to 9 weeks before the franchise is scheduled to open for business. Phase 1: instruction will pertain to administrative, operational, and sales/marketing matters. Phase 2 : will be on-the-job training. This training will be provided for the franchisee and one (1) to two (2) designated attendees.</li>
           <li>On-site live training typically takes place when the franchisee commences operations. Experienced trainers from Broccoli Pizza & Pasta will provide on-site training for a period of 7 days to assist the franchisee in the commencement of operations.</li>
         </ul>
       </div>
       </div>
       </div>
   <div class="card">
       <div class="card-header" data-toggle="collapse" href="#collapse8">
       <h4 class="card-link">
        <i class="fa fa-angle-double-right"></i> What is the term of the Broccoli Pizza & Pasta Franchise Systems Franchise Agreement?
        </h4>
       </div>
       <div id="collapse8" class="collapse" data-parent="#accordion">
       <div class="card-body">
         The initial term of the Franchise Agreement is five years from the date the Franchise Agreement is signed. The franchise is renewable for four additional terms of five years each, providing you have complied with the provisions of the Franchise Agreement.
       </div>
       </div>
       </div>
   <div class="card">
       <div class="card-header" data-toggle="collapse" href="#collapse9">
       <h4 class="card-link">
        <i class="fa fa-angle-double-right"></i>  What is the support provided by Broccoli Pizza & Pasta?
        </h4>
       </div>
       <div id="collapse9" class="collapse" data-parent="#accordion">
       <div class="card-body">
         <ul>
           <li>Operational Support — Broccoli Pizza & Pasta Management will provide ongoing training and support in many areas critical to the success of the franchisee’s business, including unit operations and maintenance, customer-service techniques, product ordering, suggested pricing guidelines, and administrative procedures.</li>
           <li>Site Selection — Prior to approving a site for Broccoli Pizza & Pasta outlet, Broccoli Pizza & Pasta Management will provide franchisees with clear guidelines for a suitable location. Broccoli Pizza & Pasta will require franchisees to follow these instructions to ensure that an appropriate site is located.</li>
           <li>Marketing Support — Broccoli Pizza & Pasta Management will coordinate development of advertising materials and strategies for the benefit of all members of the franchise network. It will also supply franchisees with consumer marketing plans and materials for use at the local or regional level, and retains the right to approve all local advertising materials that the franchisee chooses to develop.</li>
           <li>Purchasing — Broccoli Pizza & Pasta Management or its affiliate will negotiate quantity discounts on behalf of all of its members, passing some or all of these savings on to the franchisees.</li>
           <li>Accounting/Audit/Legal — Reporting directly to administration, this department is responsible for the financial and legal oversight of franchisees.</li>
           <li>Internal Support — The functional areas of training, purchasing, franchisee communications, and research and development are typically included in such a department.</li>
           <li>Ongoing Research and Development — Broccoli Pizza & Pasta Management will continue to research methods and techniques for franchise operations (including purchasing and promotional schemes) that enhance unit-level profitability.</li>
           <li>Overall Program Oversight — Broccoli Pizza & Pasta Management will provide the overall coordination and planning for the Broccoli Pizza & Pasta franchise system.</li>
         </ul>
       </div>
       </div>
       </div>
   <div class="card">
       <div class="card-header" data-toggle="collapse" href="#collapse10">
       <h4 class="card-link">
        <i class="fa fa-angle-double-right"></i> What is my next step?
        </h4>
       </div>
       <div id="collapse10" class="collapse" data-parent="#accordion">
       <div class="card-body">
         Simply complete the Franchise Application Form and return it to us. Upon receiving the form we will contact you. If you have any questions in the meantime, please feel free to contact us at: sales@broccoli.ae
       </div>
       </div>
       </div>
   <div class="card">
       <div class="card-header" data-toggle="collapse" href="#collapse11">
       <h4 class="card-link">
        <i class="fa fa-angle-double-right"></i>How to become a Broccoli Pizza & Pasta Business Development Agent?
        </h4>
       </div>
       <div id="collapse11" class="collapse" data-parent="#accordion">
       <div class="card-body">
         <div class="wpb_wrapper">
    <div class="news_text"><span style="color: #000000;">Pre-qualification for B.D.A- This criterion applies to individual B.D.A (Not Franchisee):</span></div>
      <ol class="list_right">
      <li>Must be in full compliance in all owned restaurants</li>
      <li>Atleast 6 months of experience in owning and managing a Broccoli Pizza &amp; Pasta</li>
      <li>Must speak the local language of the Business territory that you are operating in.</li>
      <li>Must be up to date with all royalty and marketing fee to the franchisor</li>
      <li>Willing to move to the territory and live there for a minimum of 11 months per annum.</li>
      <li>All partners of B.D.A must meet the criteria especially experience and franchise training</li>
      <li>Recommendation from the current Area Manager</li>
      </ol>
<!-- <p><a href="#">Apply Now</a></p> -->

  </div>
       </div>
       </div>
       </div>




</div>
   </div>
</div>
<p><br></p>
 </section>


   <div class="clearfix"></div>
  <!--=========== end section =========-->

  <!--=========== Footer section =========-->
   @include('includes.footer')
   <div class="clearfix"></div>
  <!--=========== end footer section =========-->

   <script>
   $(document).ready(function() {
 // executes when HTML-Document is loaded and DOM is ready
// breakpoint and up
$(window).resize(function(){
	if ($(window).width() >= 980){
      // when you hover a toggle show its dropdown menu
      $(".navbar .dropdown-toggle").hover(function () {
         $(this).parent().toggleClass("show");
         $(this).parent().find(".dropdown-menu").toggleClass("show");
       });
        // hide the menu when the mouse leaves the dropdown
      $( ".navbar .dropdown-menu" ).mouseleave(function() {
        $(this).removeClass("show");
      });

	}
});
});
   </script>
<script src="{{config('app.url')}}/assets/js/jquery.js"></script>
<script src="{{config('app.url')}}/assets/js/bootstrap.min.js"></script>
{{-- <script>
  function initMap() {


  var locations = [
  ['<strong>Broccoli Pizza & Pasta</strong><br>Barsha Heights (Tecom), Al Hawai Residence - Dubai - United Arab Emirates Dubai United Arab Emirates' ,25.098208, 55.175767, 0],
  ['<strong>Broccoli Pizza & Pasta</strong><br>Va New Complex Dubai United Arab Emirates' ,25.231590, 55.262820, 1],
  ['<strong>Broccoli Pizza & Pasta</strong><br>Shop No. 1, Building No. 856,Sheikh Zayed The First St, Khalidiyah Abu Dhabi United Arab Emirates' ,24.476813, 54.351221, 2],
  ['<strong>Broccoli Pizza & Pasta</strong><br>Adnoc Petrol Station Yas Island أبو ظبي‎ United Arab Emirates' ,24.499493, 54.588370, 3],
  ['<strong>Broccoli Pizza & Pasta</strong><br>Al Nahyan, Al Mamoura Area, Muroor Road Abu Dhabi United Arab Emirates' ,24.464337, 54.386217, 4],
  ['<strong>Broccoli Pizza & Pasta</strong><br>ADNOC Petrol Station, Al Bahia North Abu Dhabi United Arab Emirates' ,24.542207, 54.636967, 5],
  ['<strong>Broccoli Pizza & Pasta</strong><br>666 Adnoc Station Abu Dhabi United Arab Emirates', 24.741403, 54.825385, 6],
  ['<strong>Broccoli Pizza & Pasta</strong><br>Prince Muhammad Ibn Saad Ibn Abdulaziz Rd, Al Malqa Riyadh 13524 Saudi Arabia', 24.803865, 46.604797, 7],
  ['<strong>Broccoli Pizza & Pasta</strong><br>النزهة - طريق عثمان بن عفان‎ الرياض‎ Saudi Arabia', 24.757759, 46.717714, 8],
  ];

  var map = new google.maps.Map(document.getElementById('map'), {
  zoom: 8,
  center: new google.maps.LatLng(25.098208, 55.175767),
  mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var infowindow = new google.maps.InfoWindow({});

  var marker, i;

  for (i = 0; i < locations.length; i++) {
  marker = new google.maps.Marker({
  position: new google.maps.LatLng(locations[i][1], locations[i][2]),
  map: map
  });

  google.maps.event.addListener(marker, 'click', (function (marker, i) {
  return function () {
    infowindow.setContent(locations[i][0]);
    infowindow.open(map, marker);
  }
  })(marker, i));
  }
  }
      </script> --}}
  {{-- <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCCOQlODnt4FYLKx9k4ppdHA7O7tKBoa18&callback=initMap">
      </script> --}}

</body>

<!-- Mirrored from locksternsolutions.com/broccoli/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Feb 2019 17:15:04 GMT -->
</html>
