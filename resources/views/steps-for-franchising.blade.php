{{-- @php
  use App\About;
@endphp --}}

<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from locksternsolutions.com/broccoli/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Feb 2019 17:11:18 GMT -->
<head>
<title>Broccoli - Steps For Franchising</title>
  <meta charset="UTF-8">
  <meta name="keywords" content="HTML,CSS,XML,JavaScript">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Site Icons -->
  <link href="{{config('app.url')}}/assets/img/icon.jpg" type="{{config('app.url')}}/assets/img/Home-512.png" rel="icon">

  <!-- font-icon -->
  <link rel="stylesheet" href="{{config('app.url')}}/assets/font-awesome/css/font-awesome.min.css">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" type="text/css" href="{{config('app.url')}}/assets/css/bootstrap.min.css">

  <!-- Custom CSS -->
  <link rel="stylesheet" href="{{config('app.url')}}/assets/style.css">
  <link href="https://fonts.googleapis.com/css?family=Josefin+Sans|PT+Sans" rel="stylesheet">
  <!-- <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet"> -->

</head>
<body>
<!--=========== top head =========-->


<!--=========== end top head =========-->

<!--=========== Navbar section =========-->


 <!--=========== end brand section =========-->

  <!--=========== new section =========-->
  @include('includes.header')
  <!--=========== end new section =========-->

 <!--=========== Slider section =========-->



  <div class="clearfix"></div>
  <!--=========== end Slider section =========-->

  <!--=========== section =========-->

  <section class="seeps">
  <br>
<div class="container">
  <div class="row">
    <div class="col-xs-12 ">
      <nav>
        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
          <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-Month1" role="tab" aria-controls="nav-Month1" aria-selected="true">Month 1</a>
          <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-Month2" role="tab" aria-controls="nav-Month2" aria-selected="false">Month 2</a>
          <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-Month3" role="tab" aria-controls="nav-Month3" aria-selected="false">Month 3</a>
          <a class="nav-item nav-link" id="nav-about-tab" data-toggle="tab" href="#nav-Month4" role="tab" aria-controls="nav-Month4" aria-selected="false">Month 4</a>
                      <a class="nav-item nav-link" id="nav-about-tab" data-toggle="tab" href="#nav-Month5" role="tab" aria-controls="nav-Month5" aria-selected="false">Month 5</a>
          <a class="nav-item nav-link" id="nav-about-tab" data-toggle="tab" href="#nav-Month6" role="tab" aria-controls="nav-about6" aria-selected="false">Month 6</a>
        </div>
      </nav>
      <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-Month1" role="tabpanel" aria-labelledby="nav-home-tab">
          <div class="steps">
          <h6>First Month To Do List</h6>
          <ul>
           <li><a><i class="fa fa-angle-double-right"></i></a>
           <span>Your BDA and/or Franchise coordinator will contact you to introduce herself/himself, take some time to meet with him as soon as you can to ensure that he knows how he can help you</span>
           </li>
           <li><a><i class="fa fa-angle-double-right"></i></a>
           <span>Start searching for locations between 400 -1200 sqft – remember if you find something, take a short video, some pictures of the area, note if parking is close by, and also get a Google maps location</span>
           </li>
           <li><a><i class="fa fa-angle-double-right"></i></a>
           <span>Send any Retail units that you have seen and are available to your BDA for approval (please note that it is extremely important that you check that the location is available to rent, what the rent is and that you can afford to take the space prior to asking for Broccoli Head Office to approve the site)</span>
           </li>
           <li><a><i class="fa fa-angle-double-right"></i></a>
                 <span> You will receive a copy of your franchisee operations manual, take the time to read through it</span>
           </li>
           <li><a><i class="fa fa-angle-double-right"></i></a>
                  <span> Take time to visit different outlets and understand what they do well and what they don’t do well so that you can improve your business skills.</span>
           </li>
           <li><a><i class="fa fa-angle-double-right"></i></a>
                  <span>After 3 -4 weeks you will receive your signed contract back, file this in an important place as it is your license to operate a Broccoli restaurant. At the same time you will receive a letter in both English and Arabic stating you are a licensed franchisee and this can be used in the contract for the landlords and trade license purposes.</span>
           </li>
            </ul>
          </div>
        </div>


        <div class="tab-pane fade" id="nav-Month2" role="tabpanel" aria-labelledby="nav-profile-tab">
         <div class="steps">
          <h6>Second Month To Do List</h6>
          <ul>
           <li><a><i class="fa fa-angle-double-right"></i></a>
           <span>Continue to look for a location and be in touch with your BDA every week to discuss options</span>
           </li>
           <li><a><i class="fa fa-angle-double-right"></i></a>
           <span>If you have found a unit already, then your BDA will help you negotiate and finalise the lease</span>
           </li>
           <li><a><i class="fa fa-angle-double-right"></i></a>
           <span>Have you started a company yet? Have you arranged a trade name? if not, now that you have a location it is time to start this process, this can be done at the Ministry of Economy.</span>
           </li>
           <li><a><i class="fa fa-angle-double-right"></i></a>
                 <span> Franchisee training should ideally be taken in the month or as per available schedule of training.</span>
           </li>
           <li><a><i class="fa fa-angle-double-right"></i></a>
                  <span>Once the lease has been signed, it is time to speak with your Franchise coordinator, who will arrange a time for our draftsmen to come to the site to start the drawings process</span>
           </li>
           <li><a><i class="fa fa-angle-double-right"></i></a>
                  <span>Once the drawings are completed, you will need to procure a contractor, again your franchise coordinator can help you with this, and allow you to be able to get the process moving by sending over drawings and designs to them.</span>
           </li>
            </ul>
          </div>
        </div>


        <div class="tab-pane fade" id="nav-Month3" role="tabpanel" aria-labelledby="nav-contact-tab">
            <div class="steps">
          <h6>Third Month To Do List</h6>
          <ul>
           <li><a><i class="fa fa-angle-double-right"></i></a>
           <span>By now you will have received your quotes from your contractors – now is the time for you and your BDA meet with them and negotiate the price, so that you do not pay too much for the works, and the timeframe, and contract can be signed</span>
           </li>
           <li><a><i class="fa fa-angle-double-right"></i></a>
           <span>Your franchise coordinator will introduce you to marketing to ensure that your opening soon sticker can be arranged and installed</span>
           </li>
           <li><a><i class="fa fa-angle-double-right"></i></a>
           <span>We will also now be in a position to order your kitchen equipment, which will be done by Broccoli giving you an invoice for the total amount, breaking down exactly how much item will cost (should you want a copy of the invoice from our supplier these are available), to which you will need to pay this amount to ensure delivery on time. Please make a timely payment to our accounts team to ensure that you</span>
           </li>
           <li><a><i class="fa fa-angle-double-right"></i></a>
                 <span>Please note that kitchen equipment has a long lead time of 14-16 weeks and should be ordered and paid for with at least 16 weeks lead time prior to opening.</span>
           </li>
            </ul>
          </div>
        </div>


        <div class="tab-pane fade" id="nav-Month4" role="tabpanel" aria-labelledby="nav-about-tab">
          <div class="steps">
          <h6>Fourth Month To Do List</h6>
          <ul>
           <li><a><i class="fa fa-angle-double-right"></i></a>
           <span>Your contractor will start on site, and it is important that you have weekly meetings with your BDA and Franchise coordinator to ensure that your location finishes in a timely mano</span>
           </li>
           <li><a><i class="fa fa-angle-double-right"></i></a>
           <span>Training should be conducted in this month ensuring that you are well prepared for opening your restaurant if it has not already been done before. (Ideally we recommend training to be conducted in month 2)</span>
           </li>
           <li><a><i class="fa fa-angle-double-right"></i></a>
           <span>Staff hiring, and interviews should take place in this month. Please inquire from your Franchisee coordinator about staff hiring procedures and job postings, and should you wish to do your interviews in our office then we are happy to assist, but we must be informed at least 10 days in advance</span>
           </li>
           <li><a><i class="fa fa-angle-double-right"></i></a>
                 <span>Staff visas – (We can give PRO Referral if needed) once hired staff will take time to get their visas and the process must be complete before they start their training.</span>
           </li>
            </ul>
          </div>
        </div>


        <div class="tab-pane fade" id="nav-Month5" role="tabpanel" aria-labelledby="nav-about-tab">
          <div class="steps">
          <h6>Fifth Month To Do List</h6>
          <ul>
           <li><a><i class="fa fa-angle-double-right"></i></a>
           <span>By now your restaurant is almost ready and you will need to be tying up all the loose ends</span>
           </li>
           <li><a><i class="fa fa-angle-double-right"></i></a>
           <span>Marketing will now have to begin for your unit and you should be in touch with the marketing team to prepare menus, and other such items that will be needed in your shop</span>
           </li>
           <li><a><i class="fa fa-angle-double-right"></i></a>
           <span>Your staff will be in the process of being trained at a Broccoli outlet and it is important that you are meeting with them regularly to ensure they realise how important they are to the success of your restaurant</span>
           </li>
           <li><a><i class="fa fa-angle-double-right"></i></a>
                 <span>Your inspections from the municipality will now be due and you should speak with the BDA and franchise coordinator and they will arrange times for you to meet with the relevant people who will help you to finally open the restaurant.</span>
           </li>
            </ul>
          </div>
        </div>


        <div class="tab-pane fade" id="nav-Month6" role="tabpanel" aria-labelledby="nav-about-tab">
          <div class="steps">
          <h6>Sixth Month To Do List</h6>
          <ul>
           <li><a><i class="fa fa-angle-double-right"></i></a>
           <span>Open the door and start making money</span>
           </li>
           <li><a><i class="fa fa-angle-double-right"></i></a>
           <span>Assess the capability of your staff</span>
           </li>
           <li><a><i class="fa fa-angle-double-right"></i></a>
           <span>Understand your busy times</span>
           </li>
           <li><a><i class="fa fa-angle-double-right"></i></a>
                 <span> Understand whether delivery is needed and how to ensure this is done correctly.
                      </span>
           </li>
           <li><a><i class="fa fa-angle-double-right"></i></a>
                 <span>Ensure that all standards are maintained
                      </span>
           </li>
            </ul>
          </div>
        </div>
      </div>

    </div>
  </div>


 </div>
 </section>

   <div class="clearfix"></div>
  <!--=========== end section =========-->

  <!--=========== Footer section =========-->
   @include('includes.footer')
   <div class="clearfix"></div>
  <!--=========== end footer section =========-->

   <script>
   $(document).ready(function() {
 // executes when HTML-Document is loaded and DOM is ready
// breakpoint and up
$(window).resize(function(){
	if ($(window).width() >= 980){
      // when you hover a toggle show its dropdown menu
      $(".navbar .dropdown-toggle").hover(function () {
         $(this).parent().toggleClass("show");
         $(this).parent().find(".dropdown-menu").toggleClass("show");
       });
        // hide the menu when the mouse leaves the dropdown
      $( ".navbar .dropdown-menu" ).mouseleave(function() {
        $(this).removeClass("show");
      });

	}
});
});
   </script>
<script src="{{config('app.url')}}/assets/js/jquery.js"></script>
<script src="{{config('app.url')}}/assets/js/bootstrap.min.js"></script>
</body>

<!-- Mirrored from locksternsolutions.com/broccoli/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Feb 2019 17:15:04 GMT -->
</html>
