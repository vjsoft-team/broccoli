<nav class="navbar navbar-default yamm navbar-fixed-top" id="header"> <div class="container-fluid padding-l-r">
        <div class="left-part">
            <button type="button" class="navbar-minimalize minimalize-styl-2  pull-left "><i class="fa fa-bars"></i></button>

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{config('app.url')}}/admin/home"><i class=""></i>Broccoli</a></div>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <div class="search" style="display: none;">
                <form >
                    <input type="text" class="form-control" autocomplete="off" placeholder="Write something and press enter">
                    <span class="search-close"><i class="fa fa-times"></i></span>
                </form>
            </div>

            <ul class="nav navbar-nav navbar-right navbar-top-drops">

                {{-- <li> <span class="search-icon"><i class="fa fa-search"></i></span></li> --}}

                <li ><a href="{{config('app.url')}}/admin/settings"><i class="fa fa-gear"></i></a>

                {{-- <li class="dropdown"><a href="#" class="dropdown-toggle button-wave" data-toggle="dropdown"><i class="fa fa-envelope"></i> <span class="badge badge-xs badge-info">6</span></a>

                    <ul class="dropdown-menu dropdown-lg animated flipInX">

                        <li class="notify-title">
                            3 New messages
                        </li>
                        <li class="clearfix">
                            <a href="#">
                                <span class="pull-left">
                                    <img src="images/avtar-1.jpg" alt="" class="img-circle" width="30">
                                </span>
                                <span class="block">
                                    John Doe
                                </span>
                                <span class="media-body">
                                    Lorem ipsum dolor sit amet
                                    <em>28 minutes ago</em>
                                </span>
                            </a>
                        </li>
                        <li class="clearfix">
                            <a href="#">
                                <span class="pull-left">
                                    <img src="images/avtar-2.jpg" alt="" class="img-circle" width="30">
                                </span>
                                <span class="block">
                                    John Doe
                                </span>
                                <span class="media-body">
                                    Lorem ipsum dolor sit amet
                                    <em>28 minutes ago</em>
                                </span>
                            </a>
                        </li>
                        <li class="clearfix">
                            <a href="#">
                                <span class="pull-left">
                                    <img src="images/avtar-3.jpg" alt="" class="img-circle" width="30">
                                </span>
                                <span class="block">
                                    John Doe
                                </span>
                                <span class="media-body">
                                    Lorem ipsum dolor sit amet
                                    <em>28 minutes ago</em>
                                </span>
                            </a>
                        </li>
                        <li class="read-more"><a href="#">View All Messages <i class="fa fa-angle-right"></i></a></li>
                    </ul>
                </li> --}}
                {{-- <li class="dropdown"><a href="#" class="dropdown-toggle button-wave" data-toggle="dropdown"><i class="fa fa-bell"></i> <span class="badge badge-xs badge-warning">6</span></a>

                    <ul class="dropdown-menu dropdown-lg animated flipInX">
                        <li class="notify-title">
                            3 New messages
                        </li>
                        <li class="clearfix">
                            <a href="#">
                                <span class="pull-left">
                                    <i class="fa fa-envelope"></i>
                                </span>

                                <span class="media-body">
                                    15 New Messages
                                    <em>20 Minutes ago</em>
                                </span>
                            </a>
                        </li>
                        <li class="clearfix">
                            <a href="#">
                                <span class="pull-left">
                                    <i class="fa fa-twitter"></i>
                                </span>

                                <span class="media-body">
                                    13 New Followers
                                    <em>2 hours ago</em>
                                </span>
                            </a>
                        </li>
                        <li class="clearfix">
                            <a href="#">
                                <span class="pull-left">
                                    <i class="fa fa-download"></i>
                                </span>

                                <span class="media-body">
                                    Download complete
                                    <em>2 hours ago</em>
                                </span>
                            </a>
                        </li>
                        <li class="read-more"><a href="#">View All Alerts <i class="fa fa-angle-right"></i></a></li>
                    </ul>
                </li> --}}

                <li class="dropdown"><a href="#" class="dropdown-toggle button-wave" data-toggle="dropdown"><i class="fa fa-user"></i></a>

                    <ul class="dropdown-menu dropdown-lg animated flipInX profile">

                        <li><a href="{{config('app.url')}}/admin/settings"><i class="fa fa-user"></i>My Profile</a></li>
                        <li><a href="#"><i class="fa fa-calendar"></i>My Calendar</a></li>
                        <li><a href="#"><i class="fa fa-envelope"></i>My Inbox</a></li>
                        <li><a href="#"><i class="fa fa-barcode"></i>My Task</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="fa fa-lock"></i>Screen lock</a></li>
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                      document.getElementById('logout-form').submit();">
                         <i class="fa fa-key"></i>Logout</a>
                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                             @csrf
                         </form>
                       </li>
                    </ul>
                </li>

                {{-- <li><a href="#" class="button-wave right-sidebar-toggle waves-effect waves-button waves-light"><i class="fa fa-comment" aria-hidden="true"></i></a>
                </li> --}}
            </ul>
        </div><!--/.nav-collapse -->
    </div><!--/.container-fluid -->
</nav>

{{-- <div id="right-sidebar" class="animated fadeInRight col-md-12">
    <button id="sidebar-close" class="right-sidebar-toggle sidebar-button btn btn-default m-b-md"><i class="fa fa-times" aria-hidden="true"></i>
    </button><div class="clearfix" style="height:10px;"></div>
    <ul class="basic-list">
        <li>Google Chrome <span class="pull-right label-danger label">21.8%</span></li>
        <li>Mozila Firefox <span class="pull-right label-purple label">21.8%</span></li>
        <li>Apple Safari <span class="pull-right label-success label">21.8%</span></li>
        <li>Internet Explorer <span class="pull-right label-info label">21.8%</span></li>
        <li>Opera mini <span class="pull-right label-warning label">21.8%</span></li>
        <li>Mozila Firefox <span class="pull-right label-purple label">21.8%</span></li>
    </ul>
    <div class="feed-element">
        <a href="#" class="pull-left">
            <img alt="image" class="img-circle" src="images/avtar-1.jpg">
        </a>
        <div class="media-body">
            There are many variations of passages of Lorem Ipsum available.
            <br>
            <small class="text-muted">Today 4:21 pm</small>
        </div>
    </div>
    <div class="feed-element">
        <a href="#" class="pull-left">
            <img alt="image" class="img-circle" src="images/avtar-1.jpg">
        </a>
        <div class="media-body">
            There are many variations of passages of Lorem Ipsum available.
            <br>
            <small class="text-muted">Today 4:21 pm</small>
        </div>
    </div>

    <div class="feed-activity-list">

        <div class="feed-element">
            <div>
                <small class="pull-right text-navy">1m ago</small>
                <strong>Monica Smith</strong>
                <div>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum</div>
                <small class="text-muted">Today 5:60 pm - 12.06.2014</small>
            </div>
        </div>



        <div class="feed-element">
            <div>
                <small class="pull-right">5m ago</small>
                <strong>Anna Legend</strong>
                <div>All the Lorem Ipsum generators on the Internet tend to repeat </div>
                <small class="text-muted">Yesterday 8:48 pm - 10.06.2014</small>
            </div>
        </div>
        <div class="feed-element">
            <div>
                <small class="pull-right">5m ago</small>
                <strong>Damian Nowak</strong>
                <div>The standard chunk of Lorem Ipsum used </div>
                <small class="text-muted">Yesterday 8:48 pm - 10.06.2014</small>
            </div>
        </div>


    </div>

</div> --}}
