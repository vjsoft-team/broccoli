-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5332
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table broccoli.abouts
CREATE TABLE IF NOT EXISTS `abouts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `story` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table broccoli.abouts: ~5 rows (approximately)
/*!40000 ALTER TABLE `abouts` DISABLE KEYS */;
INSERT INTO `abouts` (`id`, `story`, `image`, `created_at`, `updated_at`, `title`) VALUES
	(1, '<p>2011: The first Broccoli restaurant opened its doors in Dubai (Tecom) on December 2nd 2011, which proved to be an instant hit. </p>\r\n<p>2012: After a successful first year, Broccoli opened a second restaurant in Jumeriah.</p>\r\n <p>2013: We envisioned expanding across the UAE, from 2 restaurants to a further 9 within 12 months.</p>\r\n <p>2014: Dreams became a reality… our 9 new restaurants were opened.</p>\r\n<p>2015-18: Broccoli continues and promises to become the leading pizza franchisers in the UK and UAE. Movements over the next 4 years will be made across the region to ensure that around 300 more restaurants will be bought to you.</p>', 'ea018ef3a1843d4671b566477a38c67b.jpg', '2019-02-19 14:27:24', '2019-03-01 10:32:34', 'The  Story'),
	(2, '<p>Broccoli Pizza & pasta offers freshly prepared Italian food and personalized meals for a health- conscious community. The name Broccoli speaks for itself- this nutritious vegetable is a symbol of our dedication to healthy eating and quality produce.</p>', '59541010338dd2dccb02b4bee30c07e5.jpg', '2019-02-19 15:34:37', '2019-03-01 10:39:31', 'Who we are'),
	(3, '<p>Broccoli serves up great tasting pizzas, pastas and salads, with a focus on fresh ingredients, fast and friendly service, and an amazing customer experience. We cook our delicious meals to order, and pride ourselves on giving the customer a huge range of choices, with endless combinations available to suit every taste.</p>', '938ac6de69b34f71792ddf03a70d8e13.jpg', '2019-02-19 16:03:31', '2019-03-01 10:58:50', 'What we do'),
	(4, '<p>Our mission is simple and unique. We are genuinely committed to delivering exactly what we believe in – healthy, nutritious and affordable meals for an energetic and active community. We want to share our passion for healthy lifestyles with our local.</p>', '4aa2a856c2b0f3f579fa703f4edd2693.png', '2019-02-19 16:03:50', '2019-03-01 09:50:36', 'Our Mission'),
	(5, '<p>A big part of the fun and satisfaction of the Broccoli experience derives from ordering your own personalized meal and seeing it cooked in front of you by our friendly and professional staff. The quality of the interaction between staff and customer is hugely important to us.</p>', 'a9f7ca4987ec8a0e0fe363b17dced9c1.jpg', '2019-02-19 16:05:33', '2019-03-01 09:50:48', 'The Experience');
/*!40000 ALTER TABLE `abouts` ENABLE KEYS */;

-- Dumping structure for table broccoli.home_grids
CREATE TABLE IF NOT EXISTS `home_grids` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table broccoli.home_grids: ~6 rows (approximately)
/*!40000 ALTER TABLE `home_grids` DISABLE KEYS */;
INSERT INTO `home_grids` (`id`, `title`, `image`, `description`, `link`, `created_at`, `updated_at`) VALUES
	(1, 'Franchise', 'cd553287478772546316876b1bf83a95.jpg', 'description updated', 'steps-for-franchising', '2019-02-27 12:24:38', '2019-03-01 16:10:25'),
	(2, 'Online', '9081a87601d3ea5fd885b09c848317c0.jpg', 'description2', 'franchise-application', '2019-02-27 12:25:39', '2019-03-01 16:13:29'),
	(3, 'Menu', '8d3761fa8902d96b4b3d8ee3d01737a7.png', 'description3', 'trade-show', '2019-02-27 12:27:12', '2019-03-01 16:15:16'),
	(4, 'About us', 'e9e4257a6f040a3dc3bb71a5dec5aae6.jpg', 'description4', 'about-us', '2019-02-27 12:28:07', '2019-03-01 16:16:17'),
	(5, 'Locations', '51e025527bf4e6c85bfc6ecacfe3e563.png', 'description5', 'location', '2019-02-28 14:15:07', '2019-03-01 16:52:42'),
	(6, 'Social Media', 'bd734a953fceebdc158f40d02004f7c5.png', 'description', 'contact-us', '2019-02-28 14:16:46', '2019-03-01 16:54:17');
/*!40000 ALTER TABLE `home_grids` ENABLE KEYS */;

-- Dumping structure for table broccoli.locations
CREATE TABLE IF NOT EXISTS `locations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `carry_out` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table broccoli.locations: ~3 rows (approximately)
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` (`id`, `country`, `location`, `area`, `mobile`, `telephone`, `description`, `carry_out`, `delivery`, `latitude`, `longitude`, `created_at`, `updated_at`) VALUES
	(1, 'UAE', 'Dubai', 'Testing', '60.05.20.00.1', '03.78.40735', 'Order Inline 24 hours a day\r\n\r\nwith respect to store timings.\r\n\r\nDial to our call center Number', '10:25 am - 10:35 am', '12:30 pm - 13:37 pm', NULL, NULL, '2019-02-20 11:33:32', '2019-03-01 18:02:37'),
	(3, 'UAE', 'Sharja', 'Fog Street', '60.05.20.00.1', '03.78.40735', 'Order Inline 24 hours a day\r\n\r\nwith respect to store timings.\r\n\r\nDial to our call center Number', '10:30 am - 11:55 pm', '12:45 am  - 13:56 pm', NULL, NULL, '2019-02-20 18:05:00', '2019-03-01 17:56:50'),
	(5, 'UAE', 'Barari Mall', 'test', '60.05.20.00.1', '03.78.40735', 'Order Inline 24 hours a day\r\n\r\nwith respect to store timings.\r\n\r\nDial to our call center Number', '10:30 am- 11:45 pm', '23:55 am- 24:54 pm', '4545345435', '45345345345345', '2019-02-22 06:10:22', '2019-03-01 17:52:48'),
	(6, 'UAE', 'Abu dhabi', 'sd', '60.05.20.00.1', '03.78.40735', 'Order Inline 24 hours a day\r\n\r\nwith respect to store timings.\r\n\r\nDial to our call center Number', '10:30  am: 12:25 pm', '23:55 am : 01:27 pm', '53453654354537573', '53545453754735476354345', '2019-02-28 10:51:26', '2019-03-01 18:02:03');
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;

-- Dumping structure for table broccoli.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table broccoli.migrations: ~5 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_02_16_104943_create_settings_table', 2),
	(4, '2019_02_16_125225_create_sliders_table', 3),
	(5, '2019_02_16_125336_create_pictures_table', 4),
	(6, '2019_02_19_135638_create_abouts_table', 5),
	(7, '2019_02_20_103539_create_locations_table', 6),
	(8, '2019_02_21_152814_create_sliders_table', 7),
	(9, '2019_02_27_062140_create_home_grids_table', 8);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table broccoli.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table broccoli.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table broccoli.pictures
CREATE TABLE IF NOT EXISTS `pictures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table broccoli.pictures: ~2 rows (approximately)
/*!40000 ALTER TABLE `pictures` DISABLE KEYS */;
INSERT INTO `pictures` (`id`, `name`, `url`, `created_at`, `updated_at`) VALUES
	(1, 'image2-1.jpg', 'uploads/image2-1.jpg', '2019-02-20 16:52:48', '2019-02-20 16:52:48'),
	(2, 'image2-80x80-resize-1.jpg', 'uploads/image2-80x80-resize-1.jpg', '2019-02-20 17:01:24', '2019-02-20 17:01:24');
/*!40000 ALTER TABLE `pictures` ENABLE KEYS */;

-- Dumping structure for table broccoli.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `restaurants` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `coming_soon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table broccoli.settings: ~0 rows (approximately)
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` (`id`, `restaurants`, `coming_soon`, `created_at`, `updated_at`) VALUES
	(1, '12', '7', NULL, '2019-02-22 05:39:51');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

-- Dumping structure for table broccoli.sliders
CREATE TABLE IF NOT EXISTS `sliders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table broccoli.sliders: ~2 rows (approximately)
/*!40000 ALTER TABLE `sliders` DISABLE KEYS */;
INSERT INTO `sliders` (`id`, `image`, `title`, `description`, `link`, `created_at`, `updated_at`) VALUES
	(4, '03d0d34e11e8dca114ae67c0bfcc2378.png', 'MUSHROOM & SWISS', 'Seasoned, Savory & satisfying', 'http://www.broccolipizzaandpasta.com/franchise-application/', '2019-02-21 16:57:58', '2019-03-01 17:12:20'),
	(7, '94e744e48ad89022f2710e53d05ee8aa.png', 'MUSHROOM & SWISS', 'Seasoned, Savory & satisfying', 'http://www.broccolipizzaandpasta.com/social-media/', '2019-02-21 17:10:36', '2019-03-01 17:14:01'),
	(8, '0c13b32317cacc88ab3ba3d0daf7dfe9.png', 'MUSHROOM & SWISS', 'Seasoned, Savory & satisfying', 'http://www.broccolipizzaandpasta.com/locations/', '2019-02-21 17:12:05', '2019-03-01 17:15:01');
/*!40000 ALTER TABLE `sliders` ENABLE KEYS */;

-- Dumping structure for table broccoli.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table broccoli.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'admin@admin.com', NULL, '$2y$10$FlJ0vGbv7tRR.iVkfcdBD.8pwmGHPU8.DYjYhzemGP6tUIf7gNRKa', 'JOswlduYSVMCOREvxPRpaIqb38KpybWq4OfRT4IqPDI6GowY3GCyzdMGqDZf', '2019-02-16 10:01:59', '2019-02-16 10:01:59');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
