<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Setting;
use App\Slider;
use App\About;
use App\Location;
use App\HomeGrid;
// use Croppa;
// use File;
// use FileUpload;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
     {
         $this->middleware('auth');
     }

     //<----------------------Setting-------------------------->//

     public function settings_view()
     {
       return view('admin.admin_settings');
     }

     public function update(Request $request)
     {
       // return $request;
       $this->validate($request,[
         'restaurants' => 'required',
         'coming_soon' => 'required',
       ]);

       $data = Setting::find(1);
       $data->restaurants = $request->restaurants;
       $data->coming_soon = $request->coming_soon;
       $data->save();

       return redirect('/admin/settings')->with('message', 'Settings Updated Succesfully !');
     }

      //<----------------------Slider-------------------------->//

      public function slider_view()
      {

        return view('admin.admin_slider');
      }

      public function slider_store(Request $request)
      {
        $this->validate($request,[
         'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:20048',
         ]);

         if (request()->hasFile('image')) {
          $file = request()->file('image');
          $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
          $file->move('./slider/', $fileName);


          $desk = new Slider();
          $desk->title = $request->title;
          $desk->description = $request->description;
          $desk->image = $fileName;
          $desk->link = $request->link;
          $desk->save();

          return redirect('/admin/slider_list')->with('message', 'Slider Added Succesfully !');

      }
    }

    public function slider_list()
    {
        return view('admin.admin_slider_list');
    }
    public function slider_edit_view()
    {
        return view('admin.admin_slider_edit');
    }

    public function slider_edit($id)
    {
        $edit = Slider::find($id);
        return view('admin.admin_slider_edit')->with('edit', $edit);
    }

    public function slider_update(Request $request)
    {

      // $this->validate($request,[
      //  'image' => 'mimes:jpeg,png,jpg,gif,svg,mp4,mov,ogg,qt|max:200000',
      //
      //  ]);

       if (request()->hasFile('image')) {
        $file = request()->file('image');
        $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
        $file->move('./slider/', $fileName);

      $data = Slider::find($request->id);

      $data->title = $request->title;
      $data->description = $request->description;
      $data->image = $fileName;
      $data->link = $request->link;
      $data->save();

      return redirect('/admin/slider_list')->with('message', 'Slider Updated Succesfully !');

    }

    else {
    $data = Slider::find($request->id);
    $data->title = $request->title;
    $data->description = $request->description;
    // $data->image = $fileName;
    $data->link = $request->link;
    $data->save();

    return redirect('/admin/slider_list')->with('message', 'Slider Updated Succesfully !');
    }
  }

  public function slider_delete($id)
  {
      $list = Slider::find($id);

      $list->delete();

      return redirect('/admin/slider_list')->with('messages', 'Slider Deleted Succesfully !');
  }
//<----------------------Home Grid-------------------------->//

public function grid_view()
{
    return view('admin.admin_content');
}

//---------------Franchise--------------------//

public function franchise(Request $request)
{


 if (request()->hasFile('image')) {
  $file = request()->file('image');
  $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
  $file->move('./grid/', $fileName);


  $desk = HomeGrid::find(1);
  $desk->title = $request->title;
  $desk->description = $request->description;
  $desk->link = $request->link;
  $desk->image = $fileName;
  $desk->save();

  return redirect('/admin/home-grid')->with('message', 'Franchise Updated Succesfully !');

}

else {
$desk = HomeGrid::find(1);
$desk->title = $request->title;
$desk->description = $request->description;
$desk->link = $request->link;
$desk->save();

return redirect('/admin/home-grid')->with('message', 'Franchise Updated Succesfully !');
}
}


//<---------------Online---------------->//

public function online(Request $request)
{


 if (request()->hasFile('image')) {
  $file = request()->file('image');
  $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
  $file->move('./grid/', $fileName);


  $desk = HomeGrid::find(2);
  $desk->title = $request->title;
  $desk->description = $request->description;
  $desk->link = $request->link;
  $desk->image = $fileName;
  $desk->save();

  return redirect('/admin/home-grid')->with('message', 'Online Updated Succesfully !');

}

else {
$desk = HomeGrid::find(2);
$desk->title = $request->title;
$desk->description = $request->description;
$desk->link = $request->link;
$desk->save();

return redirect('/admin/home-grid')->with('message', 'Online Updated Succesfully !');
}
}


//<--------------Menu--------------->//

public function menu(Request $request)
{

 if (request()->hasFile('image')) {
  $file = request()->file('image');
  $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
  $file->move('./grid/', $fileName);


  $desk = HomeGrid::find(3);
  $desk->title = $request->title;
  $desk->description = $request->description;
  $desk->link = $request->link;
  $desk->image = $fileName;
  $desk->save();

  return redirect('/admin/home-grid')->with('message', 'Menu Updated Succesfully !');

}

else {
$desk = HomeGrid::find(3);
$desk->title = $request->title;
$desk->description = $request->description;
$desk->link = $request->link;
$desk->save();

return redirect('/admin/home-grid')->with('message', 'Menu Updated Succesfully !');
}
}

//<----------About us----------->//

public function About_us(Request $request)
{

 if (request()->hasFile('image')) {
  $file = request()->file('image');
  $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
  $file->move('./grid/', $fileName);


  $desk = HomeGrid::find(4);
  $desk->title = $request->title;
  $desk->description = $request->description;
  $desk->link = $request->link;
  $desk->image = $fileName;
  $desk->save();

  return redirect('/admin/home-grid')->with('message', 'About us Updated Succesfully !');

}

else {
$desk = HomeGrid::find(4);
$desk->title = $request->title;
$desk->description = $request->description;
$desk->link = $request->link;
$desk->save();

return redirect('/admin/home-grid')->with('message', 'About us Updated Succesfully !');
}
}

//<------------Locations----------------->//

public function locations(Request $request)
{

 if (request()->hasFile('image')) {
  $file = request()->file('image');
  $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
  $file->move('./grid/', $fileName);


  $desk = HomeGrid::find(5);
  $desk->title = $request->title;
  $desk->description = $request->description;
  $desk->link = $request->link;
  $desk->image = $fileName;
  $desk->save();

  return redirect('/admin/home-grid')->with('message', 'Locations Updated Succesfully !');

}

else {
$desk = HomeGrid::find(5);
$desk->title = $request->title;
$desk->description = $request->description;
$desk->link = $request->link;
$desk->save();

return redirect('/admin/home-grid')->with('message', 'Locations Updated Succesfully !');
}
}

//<----------Social Media----------------->//

public function social_media(Request $request)
{


 if (request()->hasFile('image')) {
  $file = request()->file('image');
  $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
  $file->move('./grid/', $fileName);


  $desk = HomeGrid::find(6);
  $desk->title = $request->title;
  $desk->description = $request->description;
  $desk->link = $request->link;
  $desk->image = $fileName;
  $desk->save();

  return redirect('/admin/home-grid')->with('message', 'Social Media Updated Succesfully !');

}

else {
$desk = HomeGrid::find(6);
$desk->title = $request->title;
$desk->description = $request->description;
$desk->link = $request->link;
$desk->save();

return redirect('/admin/home-grid')->with('message', 'Social Media Updated Succesfully !');
}
}



//<----------------------About Page-------------------------->//

    public function about_view()
    {
        return view('admin.admin_about');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function about_store(Request $request)
    {
      // return 'kkkk';
      // $this->validate($request,[
      //  'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:20048',
      //  ]);

     if (request()->hasFile('image')) {
      $file = request()->file('image');
      $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
      $file->move('./about/', $fileName);


      $desk = About::find(1);
      $desk->story = $request->story;
      // $desk->title = $request->title;
      $desk->image = $fileName;
      $desk->save();

      return redirect('/admin/about')->with('message', 'The Story Updated Succesfully !');

  }

  else {
    $desk = About::find(1);
    $desk->story = $request->story;
    // $desk->title = $request->title;
    $desk->save();

    return redirect('/admin/about')->with('message', 'The Story Updated Succesfully !');
  }
    }
    public function we_do(Request $request)
    {

      // $this->validate($request,[
      //  'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:20048',
      //
      //  ]);

     if (request()->hasFile('image')) {
      $file = request()->file('image');
      $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
      $file->move('./about/', $fileName);


      $desk = About::find(2);
      $desk->story = $request->story;
      // $desk->title = $request->title;
      $desk->image = $fileName;
      $desk->save();

      return redirect('/admin/about')->with('message', 'Who we are Updated Succesfully !');

  }

  else {
    $desk = About::find(2);
    $desk->story = $request->story;
    // $desk->title = $request->title;
    $desk->save();

    return redirect('/admin/about')->with('message', 'Who we are Updated Succesfully !');
  }
    }

    public function mission(Request $request)
    {

      // $this->validate($request,[
      //  'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:20048',
      //
      //  ]);

     if (request()->hasFile('image')) {
      $file = request()->file('image');
      $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
      $file->move('./about/', $fileName);


      $desk = About::find(3);
      $desk->story = $request->story;
      // $desk->title = $request->title;
      $desk->image = $fileName;
      $desk->save();

      return redirect('admin/about')->with('message', 'What we do Updated Succesfully !');

  }

  else {
    $desk = About::find(3);
    $desk->story = $request->story;
    // $desk->title = $request->title;
    $desk->save();

    return redirect('/admin/about')->with('message', 'What we do Updated Succesfully !');
  }
    }

    public function our_story(Request $request)
    {

      // $this->validate($request,[
      //  'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:20048',
      //
      //  ]);

     if (request()->hasFile('image')) {
      $file = request()->file('image');
      $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
      $file->move('./about/', $fileName);


      $desk = About::find(4);
      $desk->story = $request->story;
      // $desk->title = $request->title;
      $desk->image = $fileName;
      $desk->save();

      return redirect('/admin/about')->with('message', 'Our Mission Updated Succesfully !');

  }
  else {
    $desk = About::find(4);
    $desk->story = $request->story;
    // $desk->title = $request->title;
    $desk->save();

    return redirect('/admin/about')->with('message', 'Our Mission Updated Succesfully !');
  }
    }

    public function experience(Request $request)
    {

      // $this->validate($request,[
      //  'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:20048',
      //
      //  ]);


     if (request()->hasFile('image')) {
      $file = request()->file('image');
      $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
      $file->move('./about/', $fileName);


      $desk = About::find(5);
      $desk->story = $request->story;
      // $desk->title = $request->title;
      $desk->image = $fileName;
      $desk->save();

      return redirect('/admin/about')->with('message', ' Experience Updated Succesfully !');

  }

  else {
    $desk = About::find(5);
    $desk->story = $request->story;
    // $desk->title = $request->title;
    $desk->save();

    return redirect('/admin/about')->with('message', 'Experience Updated Succesfully !');
  }
    }


    public function location_view()
    {
        return view('admin.admin_location');
    }

    public function location_store(Request $request)
    {
      $this->validate($request,[
       'country' => 'required',
       'location' => 'required',
       'area' => 'required',
       'mobile' => 'required',
       'telephone' => 'required',
       'description' => 'required',
       'carry_out' => 'required',
       'delivery' => 'required',
       'latitude' => 'required',
       'longitude' => 'required',

       ]);

       $data = new Location();

      $data->country = $request->country;
      $data->location = $request->location;
      $data->area = $request->area;
      $data->mobile = $request->mobile;
      $data->telephone = $request->telephone;
      $data->description = $request->description;
      $data->carry_out = $request->carry_out;
      $data->delivery = $request->delivery;
      $data->latitude = $request->latitude;
      $data->longitude = $request->longitude;

      $data->save();

      return redirect('/admin/location_list')->with('message', 'Location Added Succesfully !');
    }

    public function list_location()
    {
        return view('admin.admin_location_list');
    }
    public function list_edit_location()
    {
        return view('admin.admin_location_edit');
    }
    public function edit_location($id)
    {
        $edit = Location::find($id);
        return view('admin.admin_location_edit')->with('edit', $edit);
    }

    public function update_location(Request $request)
    {

      // $this->validate($request,[
      //  'country' => 'required',
      //  'location' => 'required',
      //  'area' => 'required',
      //  'mobile' => 'required',
      //  'telephone' => 'required',
      //  'description' => 'required',
      //  'carry_out' => 'required',
      //  'delivery' => 'required',
      //  'latitude' => 'required',
      //  'longitude' => 'required',
      //
      //  ]);

      $data = Location::find($request->id);

      $data->country = $request->country;
      $data->location = $request->location;
      $data->area = $request->area;
      $data->mobile = $request->mobile;
      $data->telephone = $request->telephone;
      $data->description = $request->description;
      $data->carry_out = $request->carry_out;
      $data->delivery = $request->delivery;
      $data->latitude = $request->latitude;
      $data->longitude = $request->longitude;

      $data->save();

      // return $data;



      return redirect('admin/location_list')->with('message', 'Location Updated Succesfully !');



    }

    public function delete_location($id)
    {
        $list = Location::find($id);

        $list->delete();

        return redirect('admin/location_list')->with('messages', 'Location Deleted Succesfully !');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
