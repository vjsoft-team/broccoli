<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/about-us', function () {
    return view('about');
});

Route::get('/career', function () {
    return view('career');
});
Route::get('/contact-us', function () {
    return view('contact');
});

Route::get('/FAQ', function () {
    return view('faq');
});

Route::get('/location', function () {
    return view('location');
});

Route::get('/franchise-application', function () {
    return view('franchise-application');
});

Route::get('/steps-for-franchising', function () {
    return view('steps-for-franchising');
});

Route::get('/attend', function () {
    return view('attend');
});

Route::get('/trade-show', function () {
    return view('trade-show');
});

Route::get('/franchise-faq', function () {
    return view('franchise-faq');
});


Route::get('/home', 'HomeController@index')->name('home');


//<--------Admin Dashboard-------->//
Route::prefix('admin')->group(function () {
  Auth::routes();

  Route::get('/home', 'HomeController@admin_index')->name('admin_home');
  //<--------Admin Settings-------->//
  Route::get('/settings', 'AdminController@settings_view');

  Route::post('/settings', 'AdminController@update')->name('settingsUpdate');

  //<-----------------Admin Home-Grid------------->//

  Route::get('/home-grid', 'AdminController@grid_view');

  Route::post('/franchise', 'AdminController@franchise')->name('Franchise');

  Route::post('/online', 'AdminController@online')->name('Online');

  Route::post('/menu', 'AdminController@menu')->name('Menu');

  Route::post('/about-us', 'AdminController@About_us')->name('About');

  Route::post('/locations', 'AdminController@locations')->name('Locations');

  Route::post('/social-media', 'AdminController@social_media')->name('SocialMedia');


  //<-------Admin Slider--------->//
  Route::get('/slider', 'AdminController@slider_view');

  Route::post('/slider', 'AdminController@slider_store')->name('SliderAdd');

  Route::get('/slider_list', 'AdminController@slider_list');

  Route::get('/slider_edit', 'AdminController@slider_edit_view');

  Route::get('/slider_edit/{id}', 'AdminController@slider_edit');

  Route::post('/slider_edit/update', 'AdminController@slider_update')->name('SliderUpdate');

  Route::get('/slider/delete/{id}', 'AdminController@slider_delete');

  // Route::delete('/slider_destroy/{picture}', 'AdminController@sliderDestroy')->name('sliderDestroy');

  //<-------Admin About Page-------->//

  Route::get('/about', 'AdminController@about_view');

  Route::post('/about', 'AdminController@about_store')->name('aboutUpdate');

  Route::post('/about/we', 'AdminController@we_do')->name('We');

  Route::post('/about/mission', 'AdminController@mission')->name('Mission');

  Route::post('/about/story', 'AdminController@our_story')->name('Story');

  Route::post('/about/experience', 'AdminController@experience')->name('Experience');

  //<---------Admin Location Page--------------->//

  Route::get('/location', 'AdminController@location_view');

  Route::post('/location', 'AdminController@location_store')->name('LocationStore');

  Route::get('/location_list', 'AdminController@list_location');

  Route::get('/location_edit', 'AdminController@list_edit_location');

  Route::get('/location_edit/{id}', 'AdminController@edit_location');

  Route::post('/location/update', 'AdminController@update_location')->name('LocationUpdate');

  Route::get('/location/delete/{id}', 'AdminController@delete_location');



});

// Route::get('pictures', 'AdminController@index');
